package com.beecode.flashmotorizado.controllers;

import android.content.Context;
import android.util.Log;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.remote.LoginWebService;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Enny on 10/6/2016.
 */
public class LoginController {
    private final String TAG = LoginController.class.getSimpleName();
    private Context mContext;

    public LoginController(Context context) {
        this.mContext = context;
    }

    public JSONObject login(String email, String pass) {
        LoginWebService webService = new LoginWebService(mContext.getResources().getString(R.string.url_services_app) + "/deliveryman/login");
        String response = webService.login(email, pass);
        JSONObject jsonObject = null;
        try {
            System.out.println("response: " + response);
            jsonObject = new JSONObject(response);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e(TAG, "Push message json exception: " + e.getMessage());

        }
        return jsonObject;
    }
}
