package com.beecode.flashmotorizado.controllers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.remote.AccountWebService;
import com.beecode.flashmotorizado.remote.Codes;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class AccountController {
    private final String TAG = AccountController.class.getSimpleName();
    private Context mContext;

    public AccountController(Context context) {
        this.mContext = context;
    }

    public JSONObject signUp(String name, String email, String pass) {
        AccountWebService webService = new AccountWebService(mContext.getResources().getString(R.string.url_services_app) + "/deliveryman");
        String response = webService.signUp(name, email, pass);
        JSONObject jsonObject = null;
        try {
            System.out.println("response: " + response);
            jsonObject = new JSONObject(response);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
        return jsonObject;
    }

    public void updateProfile(String idDeliveryMan, String lat, String lng) {
        String url = mContext.getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_LAT, lat);
            jsonObject.accumulate(Codes.FIELD_LNG, lng);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(mContext);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            Log.d("answer", response.toString());
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        queue.add(jsonObjReq);
    }
}
