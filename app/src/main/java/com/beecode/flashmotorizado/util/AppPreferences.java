package com.beecode.flashmotorizado.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {
    public static final String PREFERENCE_ID_USER = "PREFERENCE_ID_USER";
    public static final String PREFERENCE_NAME_USER = "PREFERENCE_NAME_USER";
    public static final String PREFERENCE_EMAIL_USER = "PREFERENCE_EMAIL_USER";
    public static final String PREFERENCE_USER_TYPE = "PREFERENCE_USER_TYPE";
    public static final String PREFERENCE_PHOTO = "PREFERENCE_PHOTO";
    public static final String GREAT_VIBES_REGULAR = "fonts/GreatVibes-Regular.otf";
    private SharedPreferences mSharedPrefs;
    private SharedPreferences.Editor mPrefsEditor;
    private Context mContext;


    public AppPreferences(Context context) {
        mContext = context;
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());
        mPrefsEditor = mSharedPrefs.edit();
    }

    public String getPreferenceUserType() {
        return mSharedPrefs.getString(PREFERENCE_USER_TYPE, null);
    }


    public void savePreferenceUserType(String value) {
        mPrefsEditor.putString(PREFERENCE_USER_TYPE, value);
        mPrefsEditor.commit();
    }

    public String getLoginIDUser() {
        return mSharedPrefs.getString(PREFERENCE_ID_USER, null);
    }

    public void saveLoginIDUser(String value) {
        mPrefsEditor.putString(PREFERENCE_ID_USER, value);
        mPrefsEditor.commit();
    }

    public String getLoginNameUser() {
        return mSharedPrefs.getString(PREFERENCE_NAME_USER, null);
    }

    public void saveLoginNameUser(String value) {
        mPrefsEditor.putString(PREFERENCE_NAME_USER, value);
        mPrefsEditor.commit();
    }

    public String getLoginEmailUser() {
        return mSharedPrefs.getString(PREFERENCE_EMAIL_USER, null);
    }

    public void saveLoginEmailUser(String value) {
        mPrefsEditor.putString(PREFERENCE_EMAIL_USER, value);
        mPrefsEditor.commit();
    }

    public String getPhoto() {
        return mSharedPrefs.getString(PREFERENCE_PHOTO, null);
    }

    public void savePhoto(String value) {
        mPrefsEditor.putString(PREFERENCE_PHOTO, value);
        mPrefsEditor.commit();
    }


    public void removePreferences() {
        mPrefsEditor.remove(PREFERENCE_ID_USER);
        mPrefsEditor.remove(PREFERENCE_NAME_USER);
        mPrefsEditor.remove(PREFERENCE_EMAIL_USER);
        mPrefsEditor.remove(PREFERENCE_USER_TYPE);
        mPrefsEditor.remove(PREFERENCE_PHOTO);
        mPrefsEditor.commit();
    }
}
