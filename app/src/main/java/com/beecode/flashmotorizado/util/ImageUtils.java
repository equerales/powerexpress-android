package com.beecode.flashmotorizado.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Luis on 11/11/2015.
 */
public class ImageUtils {

    /**
     * Calcula el tamaño indicado para el inSampleSize dependiendo del Ancho y Alto maximo recibido para que mantenga su escala (no se distorsione)
     *
     * @param options   options tomados por el bitmap
     * @param reqWidth  Ancho maximo
     * @param reqHeight Alto Maximo
     * @return entero que indica cuanto se puede reducir
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Escala el bitmap (Aumenta o reduce) segun el maximo Ancho y maximo Alto recibido
     *
     * @param mUri     Uri de la imagen normalmente obtenida intent.getData
     * @param mWidth   Maximo Ancho permitido para la imagen
     * @param mHeight  Maximo Alto permitido para la imagen
     * @param mContext Contexto de donde se realiza el llamado
     * @return Bitmap escalado
     */
    public static Bitmap scalateBitmap(Context mContext, Uri mUri, int mWidth, int mHeight) {

        Bitmap scaled = null;

        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            InputStream input = mContext.getContentResolver().openInputStream(mUri);
            BitmapFactory.decodeStream(input, null, opts);
            int width = opts.outWidth;
            int height = opts.outHeight;
            double scaleFactor = (width > height) ? ((double) mWidth / width)
                    : ((double) mHeight / height);
            int newHeight = Math.round((float) (height * scaleFactor));
            int newWidth = Math.round((float) (width * scaleFactor));
            int scale = calculateInSampleSize(opts, newWidth, newHeight);
            input.close();
            opts.inJustDecodeBounds = false;
            opts.inTempStorage = new byte[16 * 1024];
            opts.inSampleSize = scale;
            input = mContext.getContentResolver().openInputStream(
                    mUri);
            Bitmap bitmap = BitmapFactory.decodeStream(input, null, opts);
            //int nw = (int) (bitmap.getWidth()  * (metrics.heightPixels / bitmap.getHeight()) );
            scaled = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        } catch (FileNotFoundException fe) {
            Crashlytics.logException(fe);
            System.out.println(fe.getMessage());
        } catch (IOException fe) {
            Crashlytics.logException(fe);
            System.out.println(fe.getMessage());
        }

        return scaled;
    }

    /**
     * Rota la imagen de ser necesario ocurre normalmente en fotos tomado en posicion extraña del dispositivo.
     *
     * @param mContext    context de donde se realiza el llamado.
     * @param mBitmap     Bitmap a girar
     * @param mStringPath Path del archivo
     * @return Bitmap rotado.
     */
    public static Bitmap rotateBitmapIfNeeded(Context mContext, Bitmap mBitmap, String mStringPath) {

        // Convert bitmap to byte array
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(mStringPath);
        } catch (IOException e2) {
            Crashlytics.logException(e2);
            e2.printStackTrace();

        }

        Matrix matrix = new Matrix();


        switch (exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, 1))

        {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                        mBitmap.getWidth(), mBitmap.getHeight(),
                        matrix, true);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                        mBitmap.getWidth(), mBitmap.getHeight(),
                        matrix, true);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                mBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                        mBitmap.getWidth(), mBitmap.getHeight(),
                        matrix, true);
            default:
                break;
        }


        return mBitmap;


    }

    /**
     * Convierte un bitmap a un String Base64 para ser enviado al servidor
     *
     * @param image Bitmap a ser converito
     * @return String en formato base64.
     */
    public static String getEncoded64ImageStringFromBitmap(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.NO_WRAP);
        int length = imageEncoded.length();
        for (int i = 0; i < length; i += 1024) {
            if (i + 1024 < length)
                Log.e("PRIMER ENCODED", imageEncoded.substring(i, i + 1024));
            else
                Log.e("PRIMER ENCODED", imageEncoded.substring(i, length));
        }
        return imageEncoded;
    }

    public static String getPath(Activity mContext, Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = mContext.managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        return filePath;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}
