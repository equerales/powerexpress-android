package com.beecode.flashmotorizado.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.crashlytics.android.Crashlytics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util {

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }


    public static String hashPassword(String password) {
        String hashword = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            BigInteger hash = new BigInteger(1, md5.digest());
            hashword = hash.toString(16);
        } catch (NoSuchAlgorithmException nsae) {
            Crashlytics.logException(nsae);
        }
        return pad(hashword, 32, '0');
    }

    public static String pad(String s, int length, char pad) {
        StringBuffer buffer = new StringBuffer(s);
        while (buffer.length() < length) {
            buffer.insert(0, pad);
        }
        return buffer.toString();
    }

    /**
     * Show errors to users
     */
    private static void showErrorDialog(String message, Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static String getPath(Activity mContext, Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = mContext.managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        return filePath;
    }

    /**
     * Escala el bitmap (Aumenta o reduce) segun el maximo Ancho y maximo Alto recibido
     *
     * @param mUri     Uri de la imagen normalmente obtenida intent.getData
     * @param mWidth   Maximo Ancho permitido para la imagen
     * @param mHeight  Maximo Alto permitido para la imagen
     * @param mContext Contexto de donde se realiza el llamado
     * @return Bitmap escalado
     */
    public static Bitmap scalateBitmap(Context mContext, Uri mUri, int mWidth, int mHeight) {

        Bitmap scaled = null;

        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            InputStream input = mContext.getContentResolver().openInputStream(
                    mUri);
            BitmapFactory.decodeStream(input, null, opts);
            int width = opts.outWidth;
            int height = opts.outHeight;
            double scaleFactor = (width > height) ? ((double) mWidth / width)
                    : ((double) mHeight / height);
            int newHeight = Math.round((float) (height * scaleFactor));
            int newWidth = Math.round((float) (width * scaleFactor));
            int scale = calculateInSampleSize(opts, newWidth, newHeight);
            input.close();
            opts.inJustDecodeBounds = false;
            opts.inTempStorage = new byte[16 * 1024];
            opts.inSampleSize = scale;
            input = mContext.getContentResolver().openInputStream(
                    mUri);
            Bitmap bitmap = BitmapFactory.decodeStream(input, null, opts);
            //int nw = (int) (bitmap.getWidth()  * (metrics.heightPixels / bitmap.getHeight()) );
            scaled = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        } catch (FileNotFoundException fe) {
            Crashlytics.logException(fe);
            System.out.println(fe.getMessage());
        } catch (IOException fe) {
            Crashlytics.logException(fe);
            System.out.println(fe.getMessage());
        }

        return scaled;
    }


    /**
     * Calcula el tamaño indicado para el inSampleSize dependiendo del Ancho y Alto maximo recibido para que mantenga su escala (no se distorsione)
     *
     * @param options   options tomados por el bitmap
     * @param reqWidth  Ancho maximo
     * @param reqHeight Alto Maximo
     * @return entero que indica cuanto se puede reducir
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String StringToMd5(String text) {

        java.security.MessageDigest md;
        StringBuffer sb = new StringBuffer();
        try {
            md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(text.getBytes("UTF-8"));

            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
        } catch (NoSuchAlgorithmException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        return sb.toString();

    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else
            return false;
    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getNameStatus(int currentStatus, String customer) {
        String statusName = "";
        if (currentStatus == 1) {
            statusName = customer + " hizo una orden";
        } else {
            if (currentStatus == 2) {
                statusName = "Ir hacia el Establecimiento";
            } else {
                if (currentStatus == 3) {
                    statusName = "Estoy en el Establecimiento";
                } else {
                    if (currentStatus == 4) {
                        statusName = "Camino al Cliente";
                    } else {
                        if (currentStatus == 5) {
                            statusName = "Llegue a la Dirección";
                        }
                    }
                }
            }
        }
        return statusName;
    }

    public static boolean isTokenValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() == 8;
    }


}
