package com.beecode.flashmotorizado.interfaces;

import org.json.JSONObject;

/**
 * Created by Enny on 10/6/2016.
 */
public interface LoginTaskCompleted {
    void onLoginTaskStart();


    void onLoginTaskCompleted(JSONObject jsonObject);
}
