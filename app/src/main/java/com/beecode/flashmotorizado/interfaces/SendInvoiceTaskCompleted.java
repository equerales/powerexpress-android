package com.beecode.flashmotorizado.interfaces;

import org.json.JSONObject;

/**
 * Created by enny.querales on 31/7/2016.
 */
public interface SendInvoiceTaskCompleted {
    void onSendInvoiceTaskStart();

    void onSendInvoiceTaskCompleted(JSONObject jsonObject);
}
