package com.beecode.flashmotorizado.interfaces;

import org.json.JSONObject;

/**
 * Created by enny.querales on 9/6/2016.
 */
public interface AccountTaskCompleted {
    void onAccountTaskStart();


    void onAccountTaskCompleted(JSONObject jsonObject);
}

