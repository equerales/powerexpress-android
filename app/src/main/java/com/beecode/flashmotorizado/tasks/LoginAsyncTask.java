package com.beecode.flashmotorizado.tasks;

import android.app.Activity;

import com.beecode.flashmotorizado.controllers.LoginController;
import com.beecode.flashmotorizado.interfaces.LoginTaskCompleted;

import org.json.JSONObject;

/**
 * Created by Enny on 10/6/2016.
 */
public class LoginAsyncTask extends BaseAsyncTask {

    private LoginTaskCompleted listener;

    public LoginAsyncTask(Activity activity, LoginTaskCompleted callBack) {
        super(activity);
        this.listener = callBack;
    }

    @Override
    protected void onPreExecute() {
        listener.onLoginTaskStart();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        LoginController controller = new LoginController(super.mActivity);
        return controller.login(params[0], params[1]);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        listener.onLoginTaskCompleted(jsonObject);
    }
}

