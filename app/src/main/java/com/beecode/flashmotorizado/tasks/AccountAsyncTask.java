package com.beecode.flashmotorizado.tasks;

import android.app.Activity;

import com.beecode.flashmotorizado.controllers.AccountController;
import com.beecode.flashmotorizado.interfaces.AccountTaskCompleted;

import org.json.JSONObject;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class AccountAsyncTask extends BaseAsyncTask {

    private AccountTaskCompleted listener;

    public AccountAsyncTask(Activity activity, AccountTaskCompleted callBack) {
        super(activity);
        this.listener = callBack;
    }

    @Override
    protected void onPreExecute() {
        listener.onAccountTaskStart();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        AccountController controller = new AccountController(super.mActivity);
        return controller.signUp(params[0], params[1], params[2]);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        listener.onAccountTaskCompleted(jsonObject);
    }
}

