package com.beecode.flashmotorizado.tasks;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONObject;

/**
 * Created by enny.querales on 3/4/2016.
 */
public class BaseAsyncTask extends AsyncTask<String, Void, JSONObject> {
    protected Activity mActivity;

    public BaseAsyncTask(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        return null;
    }
}
