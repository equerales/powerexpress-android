package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 8/6/2016.
 */
public class Restaurant implements Parcelable {
    private String id;
    private String name;
    private String phone;
    private String fax;
    private double lat;
    private double lng;
    private String photo;
    private String created_at;
    private String updated_at;
    private String category_id;
    private String category_name;
    private String address;

    public Restaurant() {

    }

    public Restaurant(Parcel in) {
        this.id = in.readString();
        this.category_id = in.readString();
        this.name = in.readString();
        this.phone = in.readString();
        this.fax = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.photo = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.category_name = in.readString();
        this.address = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(category_id);
        parcel.writeString(name);
        parcel.writeString(phone);
        parcel.writeString(fax);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeString(photo);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(category_name);
        parcel.writeString(address);
    }

    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {

        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
