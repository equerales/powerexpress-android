package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 8/6/2016.
 */
public class Department implements Parcelable {

    private String id;
    private String name;
    private String description;
    private String photo;
    private Restaurant restaurant;

    public Department(){

    }

    public Department(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.photo = in.readString();
        this.restaurant = in.readParcelable(Restaurant.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(photo);
        parcel.writeParcelable(restaurant, flags);
 /*       parcel.writeInt(restaurant_id);
        parcel.writeString(restaurant_name);*/
    }

    public static final Parcelable.Creator<Department> CREATOR = new Parcelable.Creator<Department>() {

        public Department createFromParcel(Parcel in) {
            return new Department(in);
        }

        public Department[] newArray(int size) {
            return new Department[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
