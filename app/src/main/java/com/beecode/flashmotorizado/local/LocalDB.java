package com.beecode.flashmotorizado.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.beecode.flashmotorizado.R;
import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Locale;

/**
 * Created by enny.querales on 7/6/2016.
 */
public class LocalDB extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "flashmotorizado.db";
    private static int DATABASE_VERSION = 1;
    private String DB_PATH = "";
    private String ASSETS_DB_FOLDER = "";
    private String DELIVERY_MAN_TABLE = "delivery_man";
    private String CATEGORIES_TABLE = "categories";
    private String RESTAURANTS_TABLE = "restaurants";
    private String APARTMENTS_TABLE = "apartments";
    private String PRODUCTS_TABLE = "products";
    private String PED_TABLE = "pedidos";
    private String PED_DETAIL_TABLE = "pedidos_detail";

    StringBuffer DB_PATH_B = new StringBuffer();

    private Context mContext;
    private SQLiteDatabase db;

    public LocalDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        DB_PATH_B = new StringBuffer();
        DATABASE_NAME = this.mContext.getResources().getString(R.string.DATABASE_NAME);
        DATABASE_VERSION = Integer.parseInt(this.mContext.getResources().getString(R.string.DATABASE_VERSION));
        DB_PATH = this.mContext.getResources().getString(R.string.DB_PATH);
        DB_PATH_B.append("/data/data/");
        DB_PATH_B.append(this.mContext.getPackageName());
        DB_PATH_B.append("/databases/");
        ASSETS_DB_FOLDER = this.mContext.getResources().getString(R.string.ASSETS_DB_FOLDER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DB_PATH_B = new StringBuffer();
        DATABASE_NAME = this.mContext.getResources().getString(R.string.DATABASE_NAME);
        DATABASE_VERSION = Integer.parseInt(this.mContext.getResources().getString(R.string.DATABASE_VERSION));
        DB_PATH = this.mContext.getResources().getString(R.string.DB_PATH);
        DB_PATH_B.append("/data/data/");
        DB_PATH_B.append(this.mContext.getPackageName());
        DB_PATH_B.append("/databases/");
        ASSETS_DB_FOLDER = this.mContext.getResources().getString(R.string.ASSETS_DB_FOLDER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void createDB() throws IOException {
        boolean dbExist = dbExists();
        if (!dbExist) {
            super.getReadableDatabase();
            try {
                copyDatabase();
                openDataBaseCreateTables();
                if (db != null) {
                    Log.e(LocalDB.class.getName(), "Base de datos abierta");
                }

                Log.i(LocalDB.class.getName(), "La base de datos creada");
            } catch (SQLException ex) {
                Crashlytics.logException(ex);
                Log.e(LocalDB.class.getName(), "No se pudo crear la base de datos");
            }
        } else {
            copyDatabaseToExternalStorage();
            openDataBase();
            Log.i(LocalDB.class.getName(), "La base de datos ya esta creada");
        }
    }

    /**
     * Copiamos la base de datos del archivo incluido en la carpeta assets
     *
     * @throws java.io.IOException No pudo encontrar el archivo con la base de datos precargada
     */
    private void copyDatabase() throws IOException {
        String[] dbFiles = mContext.getAssets().list(ASSETS_DB_FOLDER);
        String outFileName = DB_PATH_B + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        for (int i = 0; i < dbFiles.length; i++) {
            InputStream myInput = mContext.getAssets().open(ASSETS_DB_FOLDER + "/" + dbFiles[i]);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myInput.close();
        }
        myOutput.flush();
        myOutput.close();
    }

    private void copyDatabaseToExternalStorage() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            if (sd.canWrite()) {
                String currentDBPath = DB_PATH_B + DATABASE_NAME;
                String backupDBPath = "/Android/data/" + DATABASE_NAME;
                File currentDB = new File(DB_PATH, DATABASE_NAME);
                File backupDB = new File(sd, backupDBPath);
                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                } else {
                    Log.e("SQL Helper", "database not found to copy");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            Log.e("SQL Helper", "database not copy to out storage");
        }
    }

    /**
     * Evaluamos si existe la base de datos. Para no sobreescribir la base de
     * datos cada vez que abrimos la aplicación
     *
     * @return Si existe o no la base de datos
     */
    private boolean dbExists() {
        SQLiteDatabase db = null;
        try {
            String dbPath = DB_PATH + DATABASE_NAME;
            db = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
            db.setLocale(Locale.getDefault());
            db.setLockingEnabled(true);
            db.setVersion(DATABASE_VERSION);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            Log.e("SQL Helper", "database not found");
        }
        if (db != null) {
            db.close();
        }
        return db != null ? true : false;
    }

    public void openDataBase() throws SQLException {
        String path = DB_PATH_B + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    public void openDataBaseCreateTables() throws SQLException {
        String path = DB_PATH_B + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);

    }

    public void saveDeliveryMan(DeliveryMan deliveryMan) {
        ContentValues mContentValues = new ContentValues();
        mContentValues.put("id", deliveryMan.getId());
        mContentValues.put("first_name", deliveryMan.getFirst_name());
        mContentValues.put("email", deliveryMan.getEmail());
        mContentValues.put("password", deliveryMan.getPassword());
        mContentValues.put("created_at", deliveryMan.getCreated_at());
        mContentValues.put("updated_at", deliveryMan.getUpdated_at());
        db.insert(DELIVERY_MAN_TABLE, null, mContentValues);
    }

    public void getDeliveryMen() {
        String selectQuery = "SELECT * FROM " + DELIVERY_MAN_TABLE;
        //get the cursor you're going to use
        Cursor cursor = db.rawQuery(selectQuery, null);
        //you should always use the try catch statement incase
        //something goes wrong when trying to read the data
        try {
            // looping through all rows and adding to list
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex("first_name"));
            }
        } catch (SQLiteException e) {
            Crashlytics.logException(e);
            Log.d("SQL Error", e.getMessage());
        } finally {
            //release all your resources
            cursor.close();
        }
    }

}
