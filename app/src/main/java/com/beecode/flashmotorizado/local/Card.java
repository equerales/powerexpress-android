package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class Card implements Parcelable {

    private String id;
    private String cardNumber;
    private String nameCard;
    private String expDate;
    private String ccv;
    private String type;
    private Customer customer;

    public Card() {

    }


    public Card(Parcel in) {
        this.id = in.readString();
        this.cardNumber = in.readString();
        this.nameCard = in.readString();
        this.expDate = in.readString();
        this.ccv = in.readString();
        this.type = in.readString();
        this.customer = in.readParcelable(Customer.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(cardNumber);
        parcel.writeString(nameCard);
        parcel.writeString(expDate);
        parcel.writeString(ccv);
        parcel.writeString(type);
        parcel.writeParcelable(customer, flags);
    }

    public static final Parcelable.Creator<Card> CREATOR = new Parcelable.Creator<Card>() {

        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNameCard() {
        return nameCard;
    }

    public void setNameCard(String nameCard) {
        this.nameCard = nameCard;
    }

    public String getCcv() {
        return ccv;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }
}
