package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 8/6/2016.
 */
public class Pedido implements Parcelable, Comparable<Pedido> {

    private String id;
    private Customer customer;
    private DeliveryMan delivery;
    private Restaurant restaurant;
    private String created_at;
    private double subtotal;
    private double ship;
    private double tax;
    private double total;
    private Address address;
    private int cancelled;
    private Card card;
    private int status;
    private String distance;

    public Pedido() {
    }


    public Pedido(Parcel in) {
        this.id = in.readString();
        this.customer = in.readParcelable(Customer.class.getClassLoader());
        this.restaurant = in.readParcelable(Restaurant.class.getClassLoader());
        this.created_at = in.readString();
        this.subtotal = in.readDouble();
        this.ship = in.readDouble();
        this.tax = in.readDouble();
        this.total = in.readDouble();
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.card = in.readParcelable(Card.class.getClassLoader());
        this.status = in.readInt();
        this.cancelled = in.readInt();
        this.delivery = in.readParcelable(DeliveryMan.class.getClassLoader());
        this.distance = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeParcelable(customer, flags);
        parcel.writeParcelable(restaurant, flags);
        parcel.writeString(created_at);
        parcel.writeDouble(subtotal);
        parcel.writeDouble(ship);
        parcel.writeDouble(tax);
        parcel.writeDouble(total);
        parcel.writeParcelable(address, flags);
        parcel.writeParcelable(card, flags);
        parcel.writeInt(status);
        parcel.writeInt(cancelled);
        parcel.writeParcelable(delivery, flags);
        parcel.writeString(distance);
    }

    public static final Parcelable.Creator<Pedido> CREATOR = new Parcelable.Creator<Pedido>() {

        public Pedido createFromParcel(Parcel in) {
            return new Pedido(in);
        }

        public Pedido[] newArray(int size) {
            return new Pedido[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getShip() {
        return ship;
    }

    public void setShip(double ship) {
        this.ship = ship;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DeliveryMan getDelivery() {
        return delivery;
    }

    public void setDelivery(DeliveryMan delivery) {
        this.delivery = delivery;
    }

    @Override
    public int compareTo(Pedido o) {
        String a=new String(String.valueOf(this.getCreated_at()));
        String b=new String(String.valueOf(o.getCreated_at()));
        return a.compareTo(b);
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getCancelled() {
        return cancelled;
    }

    public void setCancelled(int cancelled) {
        this.cancelled = cancelled;
    }
}
