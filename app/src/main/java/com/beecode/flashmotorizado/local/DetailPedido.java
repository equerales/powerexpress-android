package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

import com.crashlytics.android.Crashlytics;

/**
 * Created by enny.querales on 8/6/2016.
 */
public class DetailPedido implements Parcelable {

    private int id;
    private Pedido pedido;
    private Department department;
    private Product product;
    private int quantity;
    private String note;

    public DetailPedido() {

    }

    public DetailPedido(Parcel in) {
        this.id = in.readInt();
        this.pedido = in.readParcelable(Pedido.class.getClassLoader());
        this.department = in.readParcelable(Department.class.getClassLoader());
        this.product = in.readParcelable(Product.class.getClassLoader());
        this.quantity = in.readInt();
        this.note = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(id);
        parcel.writeParcelable(pedido, flags);
        parcel.writeParcelable(department, flags);
        parcel.writeParcelable(product, flags);
        parcel.writeInt(quantity);
        parcel.writeString(note);
    }

    public static final Parcelable.Creator<DetailPedido> CREATOR = new Parcelable.Creator<DetailPedido>() {

        public DetailPedido createFromParcel(Parcel in) {
            return new DetailPedido(in);
        }

        public DetailPedido[] newArray(int size) {
            return new DetailPedido[size];
        }
    };


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        DetailPedido that;
        try {
            that = (DetailPedido) other;
            if (other != null && this != null) {
                if (that.getProduct().getName() != null && this.getProduct().getName() != null) {
                    return that.getProduct().equals(this.getProduct());
                } else {
                    return (((this.getProduct() == that.getProduct())
                            && (this.getProduct().getName().equals(that.getProduct().getName()))));
                }
            } else {
                return this == other;
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.product != null ? this.product.hashCode() : 0);
        return hash;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}



