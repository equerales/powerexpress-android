package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 8/6/2016.
 */
public class Product implements Parcelable {

    private String id;
    private String name;
    private String description;
    private String reference;
    private String price;
    private String photo;
    private String created_at;
    private String updated_at;
    private Department department;

    public Product() {

    }

    public Product(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.reference = in.readString();
        this.price = in.readString();
        this.photo = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.department = in.readParcelable(Department.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(reference);
        parcel.writeString(price);
        parcel.writeString(photo);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeParcelable(department, flags);
    }

    public static final Parcelable.Creator<Department> CREATOR = new Parcelable.Creator<Department>() {

        public Department createFromParcel(Parcel in) {
            return new Department(in);
        }

        public Department[] newArray(int size) {
            return new Department[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

}
