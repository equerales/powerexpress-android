package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class Address implements Parcelable {
    private String id;
    private String address;
    private String city;
    private String province;
    private String zipCode;
    private String reference;
    private String phone;
    private double lat = 0;
    private double lng = 0;
    private Customer customer;

    public Address() {

    }

    public Address(Parcel in) {
        this.id = in.readString();
        this.address = in.readString();
        this.city = in.readString();
        this.province = in.readString();
        this.zipCode = in.readString();
        this.reference = in.readString();
        this.phone = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.customer = in.readParcelable(Customer.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(address);
        parcel.writeString(city);
        parcel.writeString(province);
        parcel.writeString(zipCode);
        parcel.writeString(reference);
        parcel.writeString(phone);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeParcelable(customer, flags);
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {

        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
