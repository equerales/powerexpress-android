package com.beecode.flashmotorizado.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class Customer implements Parcelable {
    private String id;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private String photo;
    private String created_at;
    private String updated_at;
    private String phone;
    private Address address;

    public Customer() {

    }

    public Customer(Parcel in) {
        this.id = in.readString();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.photo = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.phone = in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(photo);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(phone);
        parcel.writeParcelable(address, flags);
    }

    public static final Parcelable.Creator<Customer> CREATOR = new Parcelable.Creator<Customer>() {

        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAddressComplete(){
        return getAddress().getAddress() + " " + getAddress().getCity() + " " + getAddress().getZipCode() + " " + getAddress().getProvince();
    }
}
