package com.beecode.flashmotorizado.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.Util;
import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

/**
 * Created by enny.querales on 25/5/2016.
 */
public class ForgotPasswordActivity extends AppCompatActivity {

    private static final String TAG = "SelectPaymentMethodActivity";
    private Toolbar toolbar;
    private Activity mActivity;
    private AppPreferences appPreferences;
    private TextView mTextEmail;
    private TextView mTextToken;
    private TextView mTextPass;
    private TextView mTextRepeatPass;
    private Button mButtonSend;
    private LinearLayout mLinearTokenPass;
    private ProgressBar mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mActivity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_passwrod);
        appPreferences = new AppPreferences(mActivity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        mProgressView = (ProgressBar) findViewById(R.id.progress_bar);
        mTextEmail = (TextView) findViewById(R.id.forgot_email);
        mTextToken = (TextView) findViewById(R.id.forgot_token);
        mTextPass = (TextView) findViewById(R.id.forgot_password);
        mTextRepeatPass = (TextView) findViewById(R.id.forgot_repeat_password);
        mButtonSend = (Button) findViewById(R.id.forgot_button);
        mLinearTokenPass = (LinearLayout) findViewById(R.id.linear_token_pass);

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTextEmail.setError(null);
                mTextToken.setError(null);
                mTextPass.setError(null);
                mTextRepeatPass.setError(null);
                boolean cancel = false;
                View focusView = null;

                if (mTextEmail.getVisibility() == View.VISIBLE) {
                    if (TextUtils.isEmpty(mTextEmail.getText().toString())) {
                        mTextEmail.setError(getString(R.string.error_field_required));
                        focusView = mTextEmail;
                        cancel = true;
                    } else if (!Util.isEmailValid(mTextEmail.getText().toString())) {
                        mTextEmail.setError(getString(R.string.error_invalid_email));
                        focusView = mTextEmail;
                        cancel = true;
                    }

                    if (cancel) {
                        // There was an error; don't attempt login and focus the first
                        // form field with an error.
                        focusView.requestFocus();
                    } else {
                        // Show a progress spinner, and kick off a background task to
                        // perform the user login attempt.
                        Util.hideKeyboard(mTextEmail, mActivity);
                        sendToken(mTextEmail.getText().toString());
                    }
                } else {
                    if (mTextToken.getVisibility() == View.VISIBLE) {
                        if (!TextUtils.isEmpty(mTextRepeatPass.getText().toString()) && !TextUtils.isEmpty(mTextPass.getText().toString())) {
                            if (!mTextRepeatPass.getText().toString().equals(mTextPass.getText().toString())) {
                                mTextRepeatPass.setError(getString(R.string.password_not_math));
                                focusView = mTextRepeatPass;
                                cancel = true;
                            }
                        }

                        if (!TextUtils.isEmpty(mTextRepeatPass.getText().toString()) && !Util.isPasswordValid(mTextRepeatPass.getText().toString())) {
                            mTextRepeatPass.setError(getString(R.string.password_too_short));
                            focusView = mTextRepeatPass;
                            cancel = true;
                        }

                        if (!TextUtils.isEmpty(mTextPass.getText().toString()) && !Util.isPasswordValid(mTextPass.getText().toString())) {
                            mTextPass.setError(getString(R.string.password_too_short));
                            focusView = mTextPass;
                            cancel = true;
                        }


                        if (TextUtils.isEmpty(mTextRepeatPass.getText().toString())) {
                            mTextRepeatPass.setError(getString(R.string.error_field_required));
                            focusView = mTextRepeatPass;
                            cancel = true;
                        }

                        if (TextUtils.isEmpty(mTextPass.getText().toString())) {
                            mTextPass.setError(getString(R.string.error_field_required));
                            focusView = mTextPass;
                            cancel = true;
                        }

                        if (TextUtils.isEmpty(mTextToken.getText().toString())) {
                            mTextToken.setError(getString(R.string.error_field_required));
                            focusView = mTextToken;
                            cancel = true;
                        } else if (!Util.isTokenValid(mTextToken.getText().toString())) {
                            mTextToken.setError(getString(R.string.error_invalid_token));
                            focusView = mTextToken;
                            cancel = true;
                        }

                        if (cancel) {
                            // There was an error; don't attempt login and focus the first
                            // form field with an error.
                            focusView.requestFocus();
                        } else {
                            // Show a progress spinner, and kick off a background task to
                            // perform the user login attempt.
                            Util.hideKeyboard(mTextToken, mActivity);
                            Util.hideKeyboard(mTextPass, mActivity);
                            Util.hideKeyboard(mTextRepeatPass, mActivity);
                            repeatPassword(mTextToken.getText().toString(), mTextPass.getText().toString());
                        }
                    }
                }
            }
        });
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.forgot_radio_with_token:
                if (checked) {
                    mTextEmail.setVisibility(View.GONE);
                    mLinearTokenPass.setVisibility(View.VISIBLE);
                    mButtonSend.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.forgot_radio_without_token:
                if (checked) {
                    mLinearTokenPass.setVisibility(View.GONE);
                    mTextEmail.setVisibility(View.VISIBLE);
                    mButtonSend.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    public void sendToken(String email) {
        showProgress(true);
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/sendtoken?" + Codes.FIELD_EMAIL + "=" + email;
        System.out.println(url);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            showProgress(false);
                            Log.d("answer", response.toString());
                            if (mSuccess) {
                                mTextEmail.setVisibility(View.GONE);
                                mLinearTokenPass.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(mActivity, mMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }


    public void repeatPassword(String token, String password) {
        showProgress(true);
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/resetpassword?" + Codes.FIELD_TOKEN + "=" + token + "&" + Codes.FIELD_PASSWORD + "=" + password;
        System.out.println(url);
        JSONObject jsonObject = new JSONObject();

        RequestQueue queue = Volley.newRequestQueue(mActivity);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            showProgress(false);
                            Log.d("answer", response.toString());
                            if (mSuccess) {
                                Toast.makeText(mActivity, mMessage, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(mActivity, mMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

