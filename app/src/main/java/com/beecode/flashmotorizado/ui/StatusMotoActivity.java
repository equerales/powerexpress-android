package com.beecode.flashmotorizado.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.ui.fragments.CommentsFragment;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.ImageUtils;
import com.beecode.flashmotorizado.util.Util;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by enny.querales on 31/5/2016.
 */
public class StatusMotoActivity extends AppCompatActivity implements View.OnClickListener, CommentsFragment.CommentsFragmentDialogListener {
    private StatusMotoActivity mActivity;
    private android.support.v7.widget.Toolbar toolbar;
    private Pedido ped;
    private AppPreferences appPreferences;
    private ToggleButton mToogleAcceptOrder;
    private ToggleButton mToogleGoEstablishment;
    private ToggleButton mToogleIamEstablishment;
    private ToggleButton mToogleGoClient;
    private ToggleButton mToogleArrivedAddress;
    private TextView mNameCustomer;
    private ImageView mImageCustomer;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    private String encodedProfile;
    private RelativeLayout mRelativeCallClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_moto);
        mActivity = this;
        appPreferences = new AppPreferences(mActivity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        RelativeLayout mSeeOrder = (RelativeLayout) findViewById(R.id.status_moto_see_order);
        mSeeOrder.setOnClickListener(this);

        RelativeLayout mSeeMap = (RelativeLayout) findViewById(R.id.status_moto_location_map);
        mSeeMap.setOnClickListener(this);

        mToogleAcceptOrder = (ToggleButton) findViewById(R.id.toogle_accept_order);
        mToogleAcceptOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mToogleAcceptOrder.isChecked()) {
                    nextstatus("Tu orden ha sido Aceptada");
                }
            }
        });

        mToogleGoEstablishment = (ToggleButton) findViewById(R.id.toogle_go_establishment);
        mToogleGoEstablishment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mToogleGoEstablishment.isChecked()) {
                    nextstatus("Moto hacia el Establecimiento");
                }
            }
        });

        mToogleIamEstablishment = (ToggleButton) findViewById(R.id.toogle_iam_establishment);
        mToogleIamEstablishment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mToogleIamEstablishment.isChecked()) {
                    nextstatus("Moto en el Establecimiento");
                }
            }
        });

        mToogleGoClient = (ToggleButton) findViewById(R.id.toogle_go_to_client);
        mToogleGoClient.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mToogleGoClient.isChecked()) {
                    nextstatus("Moto camino al Cliente");
                }
            }
        });

        mToogleArrivedAddress = (ToggleButton) findViewById(R.id.toogle_arrived_address);
        mToogleArrivedAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mToogleArrivedAddress.isChecked()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(StatusMotoActivity.this);
                    builder.setTitle(getResources().getString(R.string.change_status));
                    builder.setMessage(getResources().getString(R.string.sure_arrived_pedido));
                    builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            nextstatus("Tu pedido ha llegado");
                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            }
        });
        mRelativeCallClient = (RelativeLayout) findViewById(R.id.status_call_client);

        Intent i = getIntent();
        ped = i.getParcelableExtra("pedido");
        mNameCustomer = (TextView) findViewById(R.id.status_customer_name);
        mImageCustomer = (ImageView) findViewById(R.id.status_customer_image);
        if (ped != null) {
            getProfileCustomer(ped.getCustomer().getId());
        }
    }


    private void selectImage() {
        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.chooose_library),
                getResources().getString(R.string.cancel)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(StatusMotoActivity.this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals(getResources().getString(R.string.chooose_library))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                } else if (items[item].equals(getResources().getString(R.string.cancel))) {
                    onBackPressed();
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void nextstatus(final String nameStatus) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Cambiando Status...", true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/order/nextstatus";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_ORDER_ID, ped.getId());
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            JSONObject jsonObjectResult = response.getJSONObject(Codes.FIELD_RESULT);
                            if (mSuccess) {
                                int status = jsonObjectResult.getInt(Codes.FIELD_STATUS);
                                actStatus(status);
                                sendPushStatusClient(ped, status, nameStatus);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    dialog.dismiss();
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                dialog.dismiss();
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public void actStatus(int currentStatus) {
        if (currentStatus != 0) {
            switch (currentStatus) {
                case 1:
                    mToogleAcceptOrder.setChecked(true);
                    mToogleAcceptOrder.setEnabled(false);
                    mToogleGoEstablishment.setChecked(false);
                    mToogleIamEstablishment.setChecked(false);
                    mToogleGoClient.setChecked(false);
                    mToogleArrivedAddress.setChecked(false);
                    break;
                case 2:
                    mToogleAcceptOrder.setChecked(true);
                    mToogleAcceptOrder.setEnabled(false);
                    mToogleGoEstablishment.setChecked(true);
                    mToogleGoEstablishment.setEnabled(false);
                    mToogleIamEstablishment.setChecked(false);
                    mToogleGoClient.setChecked(false);
                    mToogleArrivedAddress.setChecked(false);
                    break;
                case 3:
                    mToogleAcceptOrder.setChecked(true);
                    mToogleAcceptOrder.setEnabled(false);
                    mToogleGoEstablishment.setChecked(true);
                    mToogleGoEstablishment.setEnabled(false);
                    mToogleIamEstablishment.setChecked(true);
                    mToogleIamEstablishment.setEnabled(false);
                    mToogleGoClient.setChecked(false);
                    mToogleArrivedAddress.setChecked(false);
                    break;
                case 4:
                    mToogleAcceptOrder.setChecked(true);
                    mToogleAcceptOrder.setEnabled(false);
                    mToogleGoEstablishment.setChecked(true);
                    mToogleGoEstablishment.setEnabled(false);
                    mToogleIamEstablishment.setChecked(true);
                    mToogleIamEstablishment.setEnabled(false);
                    mToogleGoClient.setChecked(true);
                    mToogleGoClient.setEnabled(false);
                    mToogleArrivedAddress.setChecked(false);
                    break;
                case 5:
                    mToogleAcceptOrder.setChecked(true);
                    mToogleAcceptOrder.setEnabled(false);
                    mToogleGoEstablishment.setChecked(true);
                    mToogleGoEstablishment.setEnabled(false);
                    mToogleIamEstablishment.setChecked(true);
                    mToogleIamEstablishment.setEnabled(false);
                    mToogleGoClient.setChecked(true);
                    mToogleGoClient.setEnabled(false);
                    mToogleArrivedAddress.setChecked(true);
                    mToogleArrivedAddress.setEnabled(false);
                    break;
                default:
                    break;
            }
        }
    }

    public void sendPushStatusClient(final Pedido ped, final int status, String nameStatus) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", getResources().getString(R.string.sending_notification), true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/push/tocustomer";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_RECIPIENTS, ped.getCustomer().getId());
            jsonObject.accumulate(Codes.FIELD_TITLE, "Cambio de Status");
            jsonObject.accumulate(Codes.FIELD_MESSAGE, nameStatus);
            jsonObject.accumulate(Codes.FIELD_STATUS, status);
            jsonObject.accumulate(Codes.FIELD_DELIVERYMAN, appPreferences.getLoginIDUser());
            jsonObject.accumulate(Codes.FIELD_ORDER, ped.getId());
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        dialog.dismiss();
                        if (status == 5) {
                            attemptSelectImage();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    private void attemptSelectImage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.load_bill));
        builder.setMessage(getResources().getString(R.string.wish_load_bill_now));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                selectImage();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                onBackPressed();
            }
        });
        builder.show();
    }

    public void getProfileCustomer(String idCustomer) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Loading...", true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = getResources().getString(R.string.url_services_app) + "/customer/" + idCustomer;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            if (mSuccess) {
                                JSONObject result = response.getJSONObject(Codes.FIELD_RESULT);
                                if (result != null) {
                                    final String first_name = result.getString(Codes.FIELD_FIRST_NAME);
                                    final String phone = result.getString(Codes.FIELD_PHONE);
                                    String photo = result.getString(Codes.FIELD_PHOTO);
                                    mRelativeCallClient.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (phone != null) {
                                                if (!phone.contains("null")) {
                                                    if (!phone.isEmpty()) {
                                                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                                        callIntent.setData(Uri.parse("tel:" + phone));
                                                        startActivity(callIntent);
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    mNameCustomer.setText((Util.getNameStatus(ped.getStatus(), first_name)).toUpperCase());
                                    //SINCE PUSH
                                    if (!photo.contains("null")) {
                                        Bitmap bitmap = ImageUtils.decodeBase64(photo);
                                        if (bitmap != null) {
                                            mImageCustomer.setImageBitmap(bitmap);
                                        }
                                    }
                                    actStatus(ped.getStatus());
                                }
                            } else {
                                String message = response.getString(Codes.FIELD_MESSAGE);
                                Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
                            }
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
                dialog.dismiss();
                Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

    @Override
    public void onClick(View v) {
        int mId = v.getId();
        switch (mId) {
            case R.id.status_moto_see_order:
                startActivity(new Intent(mActivity, DetailOrderActivity.class).putExtra("pedido", ped));
                break;
            case R.id.status_moto_location_map:
                startActivity(new Intent(mActivity, MapMotoActivity.class).putExtra("pedido", ped));
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                Uri mUri = data.getData();
                Bitmap bitmap = ImageUtils.scalateBitmap(mActivity, mUri, 300, 300);
                encodedProfile = ImageUtils.getEncoded64ImageStringFromBitmap(bitmap);
                sendInvoice(ped, encodedProfile);
            } else {
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap selectedImage = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    encodedProfile = ImageUtils.getEncoded64ImageStringFromBitmap(selectedImage);
                    sendInvoice(ped, encodedProfile);
                }
            }
        }
    }

    public void sendInvoice(final Pedido ped, String base64) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", getResources().getString(R.string.sending_invoice), true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/deliveryman/saveinvoice";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_ORDER_ID, ped.getId());
            jsonObject.accumulate(Codes.FIELD_INVOICE, base64);
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        dialog.dismiss();
                        try {
                            boolean success = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(mActivity, DetailOrderActivity.class).putExtra("pedido", ped).putExtra("type","invoice"));
                           // onBackPressed();
                            Intent i = new Intent(mActivity, DetailOrderActivity.class);
                            i.putExtra("pedido", ped);
                            i.putExtra("tipo","invoice");
                            startActivity(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                        try {
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        toolbar.inflateMenu(R.menu.menu_status);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        if (id == R.id.menu_status_cancel_order) {
            if (ped.getStatus() != 5) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                cancelOrder(ped.getId());
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setMessage(R.string.sure_cancel_order).setPositiveButton("SI", dialogClickListener)
                        .setNegativeButton("NO", dialogClickListener).show();
            }
        }
        if (id == R.id.menu_status_free_order) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Bundle mBundle = new Bundle();
                            mBundle.putParcelable("pedido", ped);
                            CommentsFragment fragment = CommentsFragment.newInstance(StatusMotoActivity.this);
                            fragment.setArguments(mBundle);
                            fragment.show(getFragmentManager(), "comments");
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setMessage(R.string.sure_free_order).setPositiveButton("SI", dialogClickListener)
                    .setNegativeButton("NO", dialogClickListener).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void cancelOrder(String idOrder) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Cancelando Orden...", true);
        String url = getResources().getString(R.string.url_services_app) + "/order/cancel/" + idOrder;
        System.out.println(url);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            dialog.dismiss();
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            if (mSuccess) {
                                sendPushCancelOrder(ped);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public void sendPushCancelOrder(final Pedido ped) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", getResources().getString(R.string.sending_notification), true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/push/tocustomer";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_RECIPIENTS, ped.getCustomer().getId());
            jsonObject.accumulate(Codes.FIELD_TITLE, "Tu Orden ha sido Cancelada");
            jsonObject.accumulate(Codes.FIELD_MESSAGE, "");
            jsonObject.accumulate(Codes.FIELD_STATUS, -1);
            jsonObject.accumulate(Codes.FIELD_DELIVERYMAN, appPreferences.getLoginIDUser());
            jsonObject.accumulate(Codes.FIELD_ORDER, ped.getId());
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        dialog.dismiss();
                        onBackPressed();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

    public void releaseOrder(String idOrder, String description) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Liberando Orden...", true);
        String url = getResources().getString(R.string.url_services_app) + "/order/" + idOrder + "/release";
        System.out.println(url);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_DESCRIPTION, description);
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        RequestQueue queue = Volley.newRequestQueue(mActivity);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            dialog.dismiss();
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            if (mSuccess) {
                                sendPushReleaseOrder(ped);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public void sendPushReleaseOrder(final Pedido ped) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", getResources().getString(R.string.sending_notification), true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/push/tocustomer";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_RECIPIENTS, ped.getCustomer().getId());
            jsonObject.accumulate(Codes.FIELD_TITLE, "Lo sentimos, el mensajero tuvo que liberar su orden");
            jsonObject.accumulate(Codes.FIELD_MESSAGE, "");
            jsonObject.accumulate(Codes.FIELD_STATUS, -2);
            jsonObject.accumulate(Codes.FIELD_DELIVERYMAN, appPreferences.getLoginIDUser());
            jsonObject.accumulate(Codes.FIELD_ORDER, ped.getId());
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        dialog.dismiss();
                        onBackPressed();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("current", 1);
        startActivity(i);
        finish();
    }

    @Override
    public void onDialogComments(String comments) {
        if (comments != null) {
            releaseOrder(ped.getId(), comments);
        }
    }
}



