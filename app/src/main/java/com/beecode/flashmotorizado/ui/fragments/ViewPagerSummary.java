package com.beecode.flashmotorizado.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.adapters.RecyclerAdapterSummary;
import com.beecode.flashmotorizado.local.Customer;
import com.beecode.flashmotorizado.local.DeliveryMan;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.local.Restaurant;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.ui.StatusMotoActivity;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by enny.querales on 10/6/2016.
 */
public class ViewPagerSummary extends Fragment {
    private View mView;
    ArrayList<Pedido> listOrders = new ArrayList<Pedido>();
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapterSummary mAdapter;
    private TextView mEmptyView;
    private ProgressBar mProgressView;
    private AppPreferences appPreferences;
    private SwipeRefreshLayout swipeContainer;

    public ViewPagerSummary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_view_pager_summary, container, false);
            initUI();
        }

        return mView;
    }

    private void initUI() {
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mEmptyView = (TextView) mView.findViewById(R.id.empty_view);
        mProgressView = (ProgressBar) mView.findViewById(R.id.progress_bar);
        appPreferences = new AppPreferences(getActivity());
        swipeContainer = (SwipeRefreshLayout) mView.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                getOrdersMoto(appPreferences.getLoginIDUser());
            }
        });
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorRed);
        getOrdersMoto(appPreferences.getLoginIDUser());
    }

    public void getOrdersMoto(String idDeliveryMan) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan + "/orders";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            if (mSuccess) {
                                listOrders.clear();
                                JSONObject mJsonResult = response.getJSONObject(Codes.FIELD_RESULT);
                                String delivery_name = mJsonResult.getString(Codes.FIELD_FIRST_NAME);
                                JSONArray mJsonArray = mJsonResult.getJSONArray(Codes.FIELD_ORDERS_HISTORY);
                                if (mJsonArray.length() > 0) {
                                    for (int i = 0; i < mJsonArray.length(); i++) {
                                        JSONObject obj = mJsonArray.getJSONObject(i);
                                        String idOrder = obj.getString(Codes.FIELD_ORDER_ID);
                                        String store_id = obj.getString(Codes.FIELD_STORE_ID);
                                        String address_id = obj.getString(Codes.FIELD_ADDRESS_ID);
                                        double subtotal = obj.getDouble(Codes.FIELD_SUBTOTAL);
                                        double ship = obj.getDouble(Codes.FIELD_SHIP);
                                        double tax = obj.getDouble(Codes.FIELD_TAX);
                                        double total = obj.getDouble(Codes.FIELD_TOTAL);
                                        int cancelled = obj.getInt(Codes.FIELD_CANCELLED);
                                        String store_name = obj.getString(Codes.FIELD_STORE_NAME);
                                        String customer_id = obj.getString(Codes.FIELD_CUSTOMER_ID);
                                        String customer_name = obj.getString(Codes.FIELD_CUSTOMER_NAME);
                                        String address = obj.getString(Codes.FIELD_CUSTOMER_ADDRESS);
                                        String created_at = obj.getString(Codes.FIELD_CREATED_DATE);
                                        String store_address = obj.getString(Codes.FIELD_ADDRESS_STORE);
                                        int current_status = obj.getInt(Codes.FIELD_CURRENT_STATUS);
                                        Pedido ped = new Pedido();
                                        ped.setId(idOrder);
                                        ped.setSubtotal(subtotal);
                                        ped.setShip(ship);
                                        ped.setTax(tax);
                                        ped.setTotal(total);
                                        ped.setCreated_at(created_at);
                                        ped.setStatus(current_status);
                                        ped.setCancelled(cancelled);
                                        Customer customer = new Customer();
                                        customer.setId(customer_id);
                                        customer.setFirst_name(customer_name);
                                        ped.setCustomer(customer);
                                        DeliveryMan delivery = new DeliveryMan();
                                        delivery.setId(appPreferences.getLoginIDUser());
                                        delivery.setFirst_name(delivery_name);
                                        ped.setDelivery(delivery);
                                        Restaurant rest = new Restaurant();
                                        rest.setId(store_id);
                                        rest.setName(store_name);
                                        ped.setRestaurant(rest);
                                        rest.setAddress(store_address);
                                        listOrders.add(ped);
                                    }

                                    Comparator<Pedido> comparador = Collections.reverseOrder();
                                    Collections.sort(listOrders, comparador);

                                    swipeContainer.setRefreshing(false);
                                    if (listOrders.size() > 0) {
                                        mLayoutManager = new LinearLayoutManager(getActivity());
                                        mRecyclerView.setLayoutManager(mLayoutManager);
                                        mAdapter = new RecyclerAdapterSummary(getActivity(), listOrders);
                                        mRecyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                        mAdapter.setOnItemClickListener(new RecyclerAdapterSummary.ClickListener() {
                                            @Override
                                            public void onItemClick(int position, View v) {
                                                Pedido ped = (Pedido) listOrders.get(position);
                                                if (ped.getCancelled() == 0) {
                                                    startActivity(new Intent(getActivity(), StatusMotoActivity.class).putExtra("pedido", ped));
                                                }
                                            }
                                        });
                                    } else {
                                        mEmptyView.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mEmptyView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                mEmptyView.setVisibility(View.VISIBLE);
                            }
                            swipeContainer.setRefreshing(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
                swipeContainer.setRefreshing(false);
                Toast.makeText(getActivity(), R.string.no_response, Toast.LENGTH_SHORT).show();
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }
}
