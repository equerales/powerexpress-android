package com.beecode.flashmotorizado.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.adapters.ListAdapterDetailtems;
import com.beecode.flashmotorizado.local.DetailPedido;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.local.Product;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.util.AnimationText;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailOrderActivity extends AppCompatActivity {
    private DetailOrderActivity mActivity;
    private android.support.v7.widget.Toolbar toolbar;
    private ProgressBar mProgressView;
    ArrayList<DetailPedido> listProducts = new ArrayList<DetailPedido>();
    private ListView mListView;
    private ListAdapterDetailtems mAdapter;
    private TextView mTextSubtotal;
    private TextView mTextShip;
    private TextView mTextTax;
    private TextView mTexTotal;
    private TextView mTextNameRestaurant;
    private TextView mTextNameAddress;
    private LinearLayout mLinearDetail;
    private TextView mToolbarTitle;
    private TextView mDetailTitle;
    private TextView mDetailTitleTotal;
    private String tipo;
    private String subtotal;
    private String ship;
    private String tax;
    private String total;
    private String restautant;
    private String address;

    public DetailOrderActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        mActivity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mDetailTitle = (TextView) findViewById(R.id.detail_title);
        mProgressView = (ProgressBar) findViewById(R.id.progress_bar);
        mListView = (ListView) findViewById(R.id.list);
        mLinearDetail = (LinearLayout) findViewById(R.id.status_client_moto_linear);
        Intent i = getIntent();
        Pedido ped = i.getParcelableExtra("pedido");
        tipo = i.getStringExtra("tipo");
        if (ped != null) {
            getDetailOrder(ped.getId());
        }
        if (tipo != null) {
            mToolbarTitle.setText("TOTALIZAR");
            mDetailTitle.setText("Factura");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void getDetailOrder(String idOrder) {
        showProgress(true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = getResources().getString(R.string.url_services_app) + "/order/" + idOrder + "/details";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            if (mSuccess) {
                                listProducts.clear();
                                JSONObject mJsonResult = response.getJSONObject(Codes.FIELD_RESULT);
                                String subtotal = mJsonResult.getString(Codes.FIELD_SUBTOTAL);
                                String ship = mJsonResult.getString(Codes.FIELD_SHIP);
                                String tax = mJsonResult.getString(Codes.FIELD_TAX);
                                String total = mJsonResult.getString(Codes.FIELD_TOTAL);
                                JSONObject mJsonStore = mJsonResult.getJSONObject(Codes.FIELD_STORE);
                                String name_restaurant = mJsonStore.getString(Codes.FIELD_NAME);
                                String address_restaurant = mJsonStore.getString(Codes.FIELD_ADDRESS);

/*                                mTextSubtotal.setText(subtotal + "$");
                                mTextShip.setText(ship + "$");
                                mTextTax.setText(tax + "$");
                                mTexTotal.setText(total + "$");
                                mTextNameRestaurant.setText(name_restaurant);
                                mTextNameAddress.setText(address_restaurant);*/

                                subtotal = mJsonResult.getString(Codes.FIELD_SUBTOTAL) + "$";
                                ship = mJsonResult.getString(Codes.FIELD_SHIP) + "$";
                                tax = mJsonResult.getString(Codes.FIELD_TAX) + "$";
                                total = mJsonResult.getString(Codes.FIELD_TOTAL) + "$";

                                restautant = mJsonStore.getString(Codes.FIELD_NAME);
                                address = mJsonStore.getString(Codes.FIELD_ADDRESS);

                                JSONArray mJsonArrayDetails = mJsonResult.getJSONArray(Codes.FIELD_DETAILS);
                                if (mJsonArrayDetails.length() > 0) {
                                    for (int i = 0; i < mJsonArrayDetails.length(); i++) {
                                        JSONObject objDet = mJsonArrayDetails.getJSONObject(i);
                                        String idProducto = objDet.getString(Codes.FIELD_ID_PRODUCT);
                                        String name = objDet.getString(Codes.FIELD_NAME);
                                        String price = objDet.getString(Codes.FIELD_PRICE);
                                        String note = objDet.optString(Codes.FIELD_NOTE);
                                        String amount = objDet.getString(Codes.FIELD_AMOUNT);
                                        int quantity = objDet.getInt(Codes.FIELD_QUANTITY);
                                        DetailPedido det = new DetailPedido();
                                        Product prod = new Product();
                                        prod.setId(idProducto);
                                        prod.setName(name);
                                        prod.setPrice(price);
                                        det.setProduct(prod);
                                        det.setQuantity(quantity);
                                        det.setNote(note);
                                        listProducts.add(det);
                                    }
                                    showProgress(false);
                                    if (listProducts.size() > 0) {
                                        mAdapter = new ListAdapterDetailtems(mActivity, listProducts, R.layout.detail_line_order);
                                        final LinearLayout footerView = (LinearLayout) ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_detail_activity, null, false);
                                        mTextSubtotal = (TextView) footerView.findViewById(R.id.detail_subtotal);
                                        mTextShip = (TextView) footerView.findViewById(R.id.detail_ship);
                                        mTextTax = (TextView) footerView.findViewById(R.id.detail_tax);
                                        mDetailTitleTotal = (TextView) footerView.findViewById(R.id.detail_title_total);
                                        mTexTotal = (TextView) footerView.findViewById(R.id.detail_total);

                                        mTextNameRestaurant = (TextView) footerView.findViewById(R.id.detail_name_restaurant);
                                        mTextNameAddress = (TextView) footerView.findViewById(R.id.detail_name_address);

                                        mTextSubtotal.setText(subtotal);
                                        mTextShip.setText(ship);
                                        mTextTax.setText(tax);
                                        mTexTotal.setText(total);

                                        if (tipo != null) {
                                            mDetailTitleTotal.setTextSize(23);
                                            mTexTotal.setTextSize(23);
                                            new AnimationText(getBaseContext(), mTexTotal);
                                            new AnimationText(getBaseContext(), mDetailTitleTotal);
                                        }
                                        mTextNameRestaurant.setText(restautant);
                                        mTextNameAddress.setText(address);

                                        mListView.addFooterView(footerView);
                                        footerView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View arg0) {
                                            }
                                        });
                                        mListView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            }
                                        });
                                        mLinearDetail.setVisibility(View.VISIBLE);
                                    }
                                }
                            } else {
                                Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            }
                            showProgress(false);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
                showProgress(false);
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

    public void onBackPressed() {
        if (tipo != null) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("current", 1);
            startActivity(i);
        } else {
            finish();
        }
    }

}
