package com.beecode.flashmotorizado.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.Customer;
import com.beecode.flashmotorizado.local.DeliveryMan;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.local.Restaurant;
import com.beecode.flashmotorizado.remote.Codes;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

public class MapMotoActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "MapMotoActivity";
    private android.support.v7.widget.Toolbar toolbar;
    private Activity mActivity;
    private Geocoder mGeocoder;
    private String mAddress;
    private LatLng cameraLocation;
    private LatLng currentLocation;
    private LatLng currentLocationClient;
    private LatLng currentLocationEstablishment;
    private GoogleMap mMap;
    boolean isSetup;
    SupportMapFragment mapFragment;
    private TextView mTextNameCustomer;
    private TextView mTextAddressCustomer;
    private TextView mTextPhoneCustomer;
    private TextView mTextNameEst;
    private TextView mTextAddressEst;
    private TextView mTextPhoneEst;
    private Customer customer;
    private DeliveryMan delivery;
    private Restaurant establishment;
    private LinearLayout mLinearCliente;
    private LinearLayout mLinearEstablishment;
    private Marker marker;

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            if (currentLocation != null && mMap != null) {
                //Only force a camera update if user has traveled 0.1 miles from last location
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15.0f));
                currentLocation = loc;
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15.0f));
            }
            currentLocation = loc;
            if (marker != null) {
                marker.remove();
            }
            marker = mMap.addMarker(new MarkerOptions().position(currentLocation).title("Marker Moto"));
        }
    };

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            Log.d("LocationListener", "got this location: " + location.toString());
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            if (!isSetup) {
                isSetup = true;
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(currentLocation).title("Marker"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15.0f));
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private GoogleMap.OnMapClickListener myMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            Log.d(TAG, "clearing focus on address text");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mActivity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_moto);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mTextNameCustomer = (TextView) findViewById(R.id.map_name_customer);
        mTextAddressCustomer = (TextView) findViewById(R.id.map_address_customer);
        mTextPhoneCustomer = (TextView) findViewById(R.id.map_phone_customer);
        mTextNameEst = (TextView) findViewById(R.id.map_name_est);
        mTextAddressEst = (TextView) findViewById(R.id.map_address_est);
        mTextPhoneEst = (TextView) findViewById(R.id.map_phone_est);
        mLinearCliente = (LinearLayout) findViewById(R.id.map_moto_linear_client);
        mLinearEstablishment = (LinearLayout) findViewById(R.id.map_moto_linear_establishment);
        mLinearCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setOnMyLocationChangeListener(myLocationChangeListener);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocationClient, 15.0f));
            }
        });
        mLinearEstablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setOnMyLocationChangeListener(null);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocationEstablishment, 15.0f));
            }
        });
        Intent i = getIntent();
        Pedido ped = i.getParcelableExtra("pedido");
        getDetailOrder(ped.getId());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void getDetailOrder(String idOrder) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Cargando Mapa...", true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = getResources().getString(R.string.url_services_app) + "/order/" + idOrder + "/details";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            if (mSuccess) {
                                JSONObject mJsonResult = response.getJSONObject(Codes.FIELD_RESULT);
                                JSONObject objCustomer = mJsonResult.getJSONObject(Codes.FIELD_CUSTOMER);
                                customer = new Customer();

                                String nameCustomer = objCustomer.getString(Codes.FIELD_CUSTOMER_NAME);
                                String phoneCustomer = objCustomer.getString(Codes.FIELD_PHONE);
                                customer.setFirst_name(nameCustomer);
                                customer.setPhone(phoneCustomer);

                                JSONObject objCustomerAddress = mJsonResult.getJSONObject(Codes.FIELD_ADDRESS);
                                String customerAddress = objCustomerAddress.getString(Codes.FIELD_ADDRESS);
                                double customerLat = objCustomerAddress.getDouble(Codes.FIELD_LATITUDE);
                                double customerLng = objCustomerAddress.getDouble(Codes.FIELD_LONGITUDE);
                                String customerCity = objCustomerAddress.getString(Codes.FIELD_CITY);
                                String customerProvince = objCustomerAddress.getString(Codes.FIELD_PROVINCE);
                                String customerZip = objCustomerAddress.getString(Codes.FIELD_ZIPCODE);
                                String customerReference = objCustomerAddress.getString(Codes.FIELD_REFERENCE);
                                com.beecode.flashmotorizado.local.Address addressCustomer = new com.beecode.flashmotorizado.local.Address();
                                addressCustomer.setAddress(customerAddress);
                                addressCustomer.setLat(customerLat);
                                addressCustomer.setLng(customerLng);
                                addressCustomer.setCity(customerCity);
                                addressCustomer.setProvince(customerProvince);
                                addressCustomer.setZipCode(customerZip);
                                addressCustomer.setReference(customerReference);
                                customer.setAddress(addressCustomer);

                                JSONObject objEstablishment = mJsonResult.getJSONObject(Codes.FIELD_STORE);
                                establishment = new Restaurant();

                                String nameEstablishment = objEstablishment.getString(Codes.FIELD_NAME);
                                String addressEstablishment = objEstablishment.getString(Codes.FIELD_ADDRESS);
                                String phoneEstablishment = objEstablishment.getString(Codes.FIELD_PHONE);
                                double latitudEstablishment = objEstablishment.getDouble(Codes.FIELD_LATITUDE);
                                double longitudEstablishment = objEstablishment.getDouble(Codes.FIELD_LONGITUDE);
                                establishment.setName(nameEstablishment);
                                establishment.setAddress(addressEstablishment);
                                establishment.setPhone("Telf.: " + phoneEstablishment);
                                establishment.setLat(latitudEstablishment);
                                establishment.setLng(longitudEstablishment);

                                mTextNameCustomer.setText(customer.getFirst_name());
                                mTextAddressCustomer.setText(customer.getAddressComplete());
                                mTextPhoneCustomer.setText("Telf.: " + customer.getPhone());

                                mTextNameEst.setText(establishment.getName());
                                mTextAddressEst.setText(establishment.getAddress());
                                mTextPhoneEst.setText(establishment.getPhone());

                                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                                mapFragment.getMapAsync(MapMotoActivity.this);
                            } else {
                                Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }
        }) {
        };
        queue.add(jsonObjReq);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        currentLocationClient = new LatLng(customer.getAddress().getLat(), (customer.getAddress().getLng()));
        mMap.addMarker(new MarkerOptions().position(currentLocationClient).title("Marker Customer").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_customer)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocationClient, 16.0f));

        currentLocationEstablishment = new LatLng(establishment.getLat(), (establishment.getLng()));
        mMap.addMarker(new MarkerOptions().position(currentLocationEstablishment).title("Marker Establishment").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    @Override
    public void onClick(View v) {
        int mId = v.getId();
        switch (mId) {
            default:
                break;
        }
    }
}
