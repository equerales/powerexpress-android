package com.beecode.flashmotorizado.ui;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.gcm.Config;
import com.beecode.flashmotorizado.gcm.GcmIntentService;
import com.beecode.flashmotorizado.local.Customer;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.local.Restaurant;
import com.beecode.flashmotorizado.location.LocationService;
import com.beecode.flashmotorizado.ui.fragments.ViewPagerNotifications;
import com.beecode.flashmotorizado.ui.fragments.ViewPagerProfile;
import com.beecode.flashmotorizado.ui.fragments.ViewPagerSummary;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MainActivity mActivity;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView mTextNotification;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private final List<Fragment> mFragmentList = new ArrayList<>();

    // Location
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mBestReading;

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;
        initUI();
        // Obtenemos el id del deliveryman que esta logged
        AppPreferences appPreferences = new AppPreferences(mActivity);
        // Inicia el servicio (inBackground) que actualiza la ubicación del motorizado en el servidor
        Intent servicio = new Intent(mActivity, LocationService.class);
        servicio.putExtra("id", appPreferences.getLoginIDUser());
        startService(servicio);
    }


    private void initUI() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        int current = getIntent().getIntExtra("current", 0);
        if (current != 0) {
            viewPager.setCurrentItem(1);
        }
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    Intent i = new Intent(MainActivity.this, GcmIntentService.class);
                    startService(i);
                } else {
                    if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        handlePushNotification(intent);
                    }
                }
            }
        };
    }

    private void handlePushNotification(Intent intent) {
        String title = intent.getStringExtra("title");
        Toast.makeText(mActivity, title, Toast.LENGTH_SHORT).show();
        String customer_id = intent.getStringExtra("customer");
        String store_photo = intent.getStringExtra("store_photo");
        String store_name = intent.getStringExtra("store_name");
        String order_id = intent.getStringExtra("order");
        String distance = intent.getStringExtra("distance");
        String shipping = intent.getStringExtra("shipping");
        Pedido pedido = new Pedido();
        Restaurant rest = new Restaurant();
        rest.setName(store_name);
        rest.setPhoto(store_photo);
        Customer customer = new Customer();
        customer.setId(customer_id);
        pedido.setId(order_id);
        pedido.setRestaurant(rest);
        pedido.setCustomer(customer);
        pedido.setShip(Double.parseDouble(shipping));
        pedido.setDistance(distance);
        viewPager.setCurrentItem(0);
        ViewPagerNotifications viewPagerNotification = (ViewPagerNotifications) mFragmentList.get(0);
        viewPagerNotification.refreshNotifications(MainActivity.this, pedido);
        changeNotification(viewPagerNotification.getQuantityNotifications());
    }

    private void setupTabIcons() {
        RelativeLayout relativeOne = (RelativeLayout) LayoutInflater.from(mActivity).inflate(R.layout.tab_layout_moto, null);
        ImageView tabOne = (ImageView) relativeOne.findViewById(R.id.tab);
        tabOne.setBackgroundDrawable(getResources().getDrawable(R.drawable.notifications));
        mTextNotification = (TextView) relativeOne.findViewById(R.id.notification);
        tabLayout.getTabAt(0).setCustomView(relativeOne);

        RelativeLayout relativeTwo = (RelativeLayout) LayoutInflater.from(mActivity).inflate(R.layout.tab_layout_moto, null);
        ImageView tabTwo = (ImageView) relativeTwo.findViewById(R.id.tab);
        tabTwo.setBackgroundDrawable(getResources().getDrawable(R.drawable.orders));
        tabLayout.getTabAt(1).setCustomView(relativeTwo);

        RelativeLayout relativeThree = (RelativeLayout) LayoutInflater.from(mActivity).inflate(R.layout.tab_layout_moto, null);
        ImageView tabThree = (ImageView) relativeThree.findViewById(R.id.tab);
        tabThree.setBackgroundDrawable(getResources().getDrawable(R.drawable.profile));
        tabLayout.getTabAt(2).setCustomView(relativeThree);

    }

    public void changeNotification(int size) {
        if (size > 0) {
            mTextNotification.setVisibility(View.VISIBLE);
            mTextNotification.setText(String.valueOf(size));
        } else {
            if (size == 0) {
                mTextNotification.setVisibility(View.GONE);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ViewPagerNotifications(), "");
        adapter.addFrag(new ViewPagerSummary(), "");
        adapter.addFrag(new ViewPagerProfile(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // return null to display only the icon
            return null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration compete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clearing the notification tray
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected void onPause() {
        // Inicia el servicio (Background) que actualiza la ubicación del motorizado en el servidor
        Intent servicio = new Intent(getBaseContext(), LocationService.class);
        stopService(servicio);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void onBackPressed() {
        ViewPagerNotifications viewPagerNotification = (ViewPagerNotifications) mFragmentList.get(0);
        int size = viewPagerNotification.getCountOrders();
        if (size > 0) {
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Alerta!");
            builder.setMessage("Tiene Notificaciones pendientes por aceptar o rechazar.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        } else {
            finish();
        }
    }
}




