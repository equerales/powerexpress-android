package com.beecode.flashmotorizado.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.gcm.GcmIntentService;
import com.beecode.flashmotorizado.interfaces.LoginTaskCompleted;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.tasks.LoginAsyncTask;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.Util;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements LoginTaskCompleted {

    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private LoginActivity mActivity;
    private TextView mTextForgotPass;
    private String mEmail;
    private String mPassword;
    private final String TAG = "LOGIN";
    private AppPreferences appPreferences;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mActivity = this;
        appPreferences = new AppPreferences(mActivity);
        initUI();
    }

    private void initUI() {
        mEmailView = (EditText) findViewById(R.id.login_email);
        mPasswordView = (EditText) findViewById(R.id.login_password);
        mTextForgotPass = (TextView) findViewById(R.id.forgot_password);

        if (appPreferences.getLoginNameUser() != null) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mTextForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });


        Button mEmailSignInButton = (Button) findViewById(R.id.login_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mProgressView = findViewById(R.id.progress_bar);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mEmailView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(mPassword) && !Util.isPasswordValid(mPassword)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Util.isEmailValid(mEmail)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Util.hideKeyboard(mPasswordView, mActivity);
            LoginAsyncTask mTask = new LoginAsyncTask(mActivity, LoginActivity.this);
            mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mEmail, mPassword);
        }
    }

    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onLoginTaskStart() {
        showProgress(true);
    }

    @Override
    public void onLoginTaskCompleted(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {
                boolean success = jsonObject.getBoolean(Codes.FIELD_SUCCESS);
                String message = jsonObject.getString(Codes.FIELD_MESSAGE);
                Log.i("Respuesta Server ", jsonObject.toString());
                if (success) {
                    JSONObject jsonResult = jsonObject.getJSONObject(Codes.FIELD_RESULT);
                    String id = jsonResult.getString(Codes.FIELD_ID);
                    String name = jsonResult.getString(Codes.FIELD_FIRST_NAME);
                    String email = jsonResult.getString(Codes.FIELD_EMAIL);
                    AppPreferences appPreferences = new AppPreferences(mActivity);
                    appPreferences.saveLoginIDUser(id);
                    appPreferences.saveLoginNameUser(name);
                    appPreferences.saveLoginEmailUser(email);
                    Toast.makeText(this, message + " " + getResources().getString(R.string.sending_token), Toast.LENGTH_LONG).show();
                    if (checkPlayServices()) {
                        registerGCM();
                    }
                } else {
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            } else {
                Toast.makeText(this.getApplicationContext(), R.string.no_response, Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e("", "Push message json exception: " + e.getMessage());
            showProgress(false);
        }
    }
}

