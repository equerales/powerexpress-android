package com.beecode.flashmotorizado.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.FlashMotorizado;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.ui.LoginActivity;
import com.beecode.flashmotorizado.ui.MainActivity;
import com.beecode.flashmotorizado.ui.dialogs.ChangePasswordFragment;
import com.beecode.flashmotorizado.ui.dialogs.EditProfileDialogFragment;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.ImageUtils;
import com.beecode.flashmotorizado.util.RoundedImageView;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by enny.querales on 11/6/2016.
 */
public class ViewPagerProfile extends Fragment implements ChangePasswordFragment.ChangePasswordFragmentDialogListener, EditProfileDialogFragment.EditProfileDialogListener {
    private View mView;
    private View mProgressView;
    private AppPreferences appPreferences;
    private TextView mTextName;
    private TextView mTextPhone;
    private RoundedImageView mImageProfile;
    private TextView mTextChangeImage;
    private RelativeLayout mEditProfile;
    private RelativeLayout mChangePassword;
    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap;
    private LinearLayout mLinearProfile;
    private SwipeRefreshLayout swipeContainer;
    private ToggleButton tgbutton;

    public ViewPagerProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_view_pager_profile, container, false);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            initUI();
        }

        return mView;
    }

    private void initUI() {
        appPreferences = new AppPreferences(getActivity());
        mTextName = (TextView) mView.findViewById(R.id.view_pager_profile_name);
        mTextPhone = (TextView) mView.findViewById(R.id.view_pager_profile_phone);
        mEditProfile = (RelativeLayout) mView.findViewById(R.id.view_pager_profile_edit);
        mChangePassword = (RelativeLayout) mView.findViewById(R.id.view_pager_change_password);
        mImageProfile = (RoundedImageView) mView.findViewById(R.id.view_pager_profile_image);
        mTextChangeImage = (TextView) mView.findViewById(R.id.view_pager_profile_change_image);
        mLinearProfile = (LinearLayout) mView.findViewById(R.id.view_pager_linear_profile);

        mTextChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        mEditProfile.setOnClickListener(null);
        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordFragment fragment = ChangePasswordFragment.newInstance(ViewPagerProfile.this);
                fragment.show(getActivity().getFragmentManager(), "change_password");
            }
        });

        mImageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView mCloseSession = (TextView) mView.findViewById(R.id.view_pager_profile_close_session);
        mCloseSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                appPreferences.removePreferences();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                ((MainActivity) getActivity()).finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.sure_signout).setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener).show();
            }
        });

        tgbutton = (ToggleButton) mView.findViewById(R.id.view_pager_profile_active);
        tgbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (tgbutton.isChecked()) {
                    tgbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.moto_inactivo));
                    updateStatus(appPreferences.getLoginIDUser(), "0");
                } else {
                    tgbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.moto_activo));
                    updateStatus(appPreferences.getLoginIDUser(), "1");
                }
            }
        });

        mProgressView = mView.findViewById(R.id.progress_bar);


        swipeContainer = (SwipeRefreshLayout) mView.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                getProfile(appPreferences.getLoginIDUser());
            }
        });
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });
        swipeContainer.setColorSchemeResources(R.color.colorRed);
        if (appPreferences.getLoginIDUser() != null) {
            getProfile(appPreferences.getLoginIDUser());
        }

    }

    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK)
            try {
                if (bitmap != null) {
                    bitmap.recycle();
                }

                Uri mUri = data.getData();
                bitmap = ImageUtils.scalateBitmap(getActivity(), mUri, 200, 200);
                mImageProfile.setImageBitmap(bitmap);
                if (bitmap != null) {
                    updatePhoto(appPreferences.getLoginIDUser());
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void getProfile(String idDeliveryMan) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            if (mSuccess) {
                                JSONObject result = response.getJSONObject(Codes.FIELD_RESULT);
                                if (result != null) {
                                    final String first_name = result.getString(Codes.FIELD_FIRST_NAME);
                                    final String phone = result.getString(Codes.FIELD_PHONE);
                                    String photo = result.getString(Codes.FIELD_PHOTO);
                                    String active = result.getString(Codes.FIELD_ACTIVE);
                                    mTextName.setText(first_name);
                                    if (!phone.contains("null")) {
                                        mTextPhone.setText("Telf.: " + phone);
                                    }

                                    if (active != null) {
                                        if (!active.contains("null")) {
                                            if (active.equalsIgnoreCase("0")) {
                                                tgbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.moto_inactivo));
                                            } else {
                                                if (active.equalsIgnoreCase("1")) {
                                                    tgbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.moto_activo));
                                                }
                                            }
                                        }
                                    }

                                    if (!photo.contains("null")) {
                                        FlashMotorizado.setPhoto(photo);
                                        appPreferences.savePhoto(photo);
                                        Bitmap bitmap = ImageUtils.decodeBase64(photo);
                                        if (bitmap != null) {
                                            mImageProfile.setImageBitmap(bitmap);
                                        }
                                    }


                                    mLinearProfile.setVisibility(View.VISIBLE);
                                    mEditProfile.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Bundle mBundle = new Bundle();
                                            mBundle.putString("name", first_name);
                                            mBundle.putString("phone", phone);
                                            EditProfileDialogFragment fragment = EditProfileDialogFragment.newInstance(ViewPagerProfile.this);
                                            fragment.setArguments(mBundle);
                                            fragment.show(getActivity().getFragmentManager(), "editar_perfil");
                                        }
                                    });
                                }
                            } else {
                                Toast.makeText(getActivity(), R.string.no_response, Toast.LENGTH_LONG).show();
                            }
                            swipeContainer.setRefreshing(false);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
                swipeContainer.setRefreshing(false);
                Toast.makeText(getActivity(), R.string.no_response, Toast.LENGTH_SHORT).show();
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

    public void updatePhoto(String idDeliveryMan) {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Cargando Foto...", true);
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;
        System.out.println(url);
        final String base64 = ImageUtils.getEncoded64ImageStringFromBitmap(bitmap);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Codes.FIELD_PHOTO, base64);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            Log.d("answer", response.toString());
                            dialog.dismiss();
                            Toast.makeText(getActivity(), getResources().getString(R.string.photo_upload), Toast.LENGTH_SHORT).show();
                            appPreferences.savePhoto(base64);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public void updateStatus(String idDeliveryMan, String active) {
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;
        System.out.println(url);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_ACTIVE, active);
        } catch (JSONException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            if (mSuccess) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.status_success), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.status_failed), Toast.LENGTH_SHORT).show();
                            }
                            System.out.println(response);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    @Override
    public void onDialoChangePassword(String password) {

    }

    @Override
    public void onDialogEditProfile(String name, String phone) {
        mTextName.setText(name);
        mTextPhone.setText("Telf.: " + phone);
    }
}
