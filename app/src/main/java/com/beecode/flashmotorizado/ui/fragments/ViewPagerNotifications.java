package com.beecode.flashmotorizado.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.adapters.RecyclerAdapterNotifications;
import com.beecode.flashmotorizado.local.Customer;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.local.Restaurant;
import com.beecode.flashmotorizado.ui.MainActivity;

import java.util.ArrayList;

/**
 * Created by enny.querales on 10/6/2016.
 */
public class ViewPagerNotifications extends Fragment {
    private View mView;
    private MainActivity mActivity;
    ArrayList<Pedido> listOrders = new ArrayList<Pedido>();
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapterNotifications mAdapter;
    private TextView mEmptyView;
    private ProgressBar mProgressView;

    public ViewPagerNotifications() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = ((MainActivity) getActivity());
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_view_pager_notifications, container, false);
            initUI();
        }

        return mView;
    }

    private void initUI() {
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mEmptyView = (TextView) mView.findViewById(R.id.empty_view);
        mProgressView = (ProgressBar) mView.findViewById(R.id.progress_bar);
        mEmptyView.setVisibility(View.VISIBLE);
        Intent intent = mActivity.getIntent();
        String customer_id = intent.getStringExtra("customer");
        String store_photo = intent.getStringExtra("store_photo");
        String store_name = intent.getStringExtra("store_name");
        String order_id = intent.getStringExtra("order");
        String distance = intent.getStringExtra("distance");
        String shipping = intent.getStringExtra("shipping");
        if (order_id != null) {
            Pedido pedido = new Pedido();
            Restaurant rest = new Restaurant();
            rest.setName(store_name);
            rest.setPhoto(store_photo);
            Customer customer = new Customer();
            customer.setId(customer_id);
            pedido.setId(order_id);
            pedido.setRestaurant(rest);
            pedido.setCustomer(customer);
            pedido.setShip(Double.parseDouble(shipping));
            pedido.setDistance(distance);
            refreshNotifications(mActivity, pedido);
            mActivity.changeNotification(getQuantityNotifications());
        }
        mAdapter = new RecyclerAdapterNotifications(mActivity, listOrders);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public int getQuantityNotifications() {
        return listOrders.size();
    }

    public void refreshNotifications(final Activity context, Pedido pedido) {
        mEmptyView.setVisibility(View.GONE);
        if (listOrders.isEmpty()) {
            mAdapter = new RecyclerAdapterNotifications(mActivity, listOrders);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
        listOrders.add(pedido);
        mAdapter.notifyDataSetChanged();

    }

    public int getCountOrders() {
        return listOrders.size();
    }
}
