package com.beecode.flashmotorizado.ui.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.Util;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by enny.querales on 23/6/2016.
 */
public class EditProfileDialogFragment extends DialogFragment {

    private EditText mEditName;
    private EditText mEditPhone;
    private Button mButtonEdit;
    private Button mButtonCancel;
    private View mProgressView;
    private String name;
    private String phone;
    private AppPreferences appPreferences;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface EditProfileDialogListener {
        public void onDialogEditProfile(String name, String phone);
    }

    private EditProfileDialogListener mListener;

    public static EditProfileDialogFragment newInstance(EditProfileDialogListener mListener) {

        EditProfileDialogFragment fragment = new EditProfileDialogFragment();
        fragment.mListener = mListener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_edit_profile);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        appPreferences = new AppPreferences(getActivity());

        wlp.gravity = Gravity.CENTER;
        //  wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        mEditName = (EditText) dialog.findViewById(R.id.edit_profile_name);
        mEditPhone = (EditText) dialog.findViewById(R.id.edit_profile_phone);
        mButtonEdit = (Button) dialog.findViewById(R.id.edit_profile_save);
        mButtonCancel = (Button) dialog.findViewById(R.id.edit_profile_cancel);
        mProgressView = dialog.findViewById(R.id.progress_bar);

        mButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptUpdateProfile();
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyboard(mEditName, getActivity());
                Util.hideKeyboard(mEditPhone, getActivity());
                dismiss();
            }
        });

        Bundle mBundle = getArguments();
        String name = mBundle.getString("name");
        String phone = mBundle.getString("phone");

        if (!name.contains("null")) {
            mEditName.setText(name);
        }

        if (!phone.contains("null")) {
            mEditPhone.setText(phone);
        }

        dialog.show();
        return dialog;
    }

    public void updateProfile(String idDeliveryMan) {
        showProgress(true);
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;
        System.out.println(url);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_FIRST_NAME, mEditName.getText().toString());
            jsonObject.accumulate(Codes.FIELD_PHONE, mEditPhone.getText().toString());
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        System.out.println(url);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();
                            appPreferences.saveLoginNameUser(mEditName.getText().toString());
                            Log.d("answer", response.toString());
                            mListener.onDialogEditProfile(mEditName.getText().toString(), mEditPhone.getText().toString());
                            Util.hideKeyboard(mEditName, getActivity());
                            Util.hideKeyboard(mEditPhone, getActivity());
                            dismiss();
                            showProgress(false);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    showProgress(false);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        queue.add(jsonObjReq);
    }

    private void attemptUpdateProfile() {

        // Reset errors.
        mEditName.setError(null);
        mEditPhone.setError(null);

        // Store values at the time of the login attempt.
        name = mEditName.getText().toString();
        phone = mEditPhone.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(phone)) {
            mEditPhone.setError(getString(R.string.error_field_required));
            focusView = mEditPhone;
            cancel = true;
        }

        if (TextUtils.isEmpty(name)) {
            mEditName.setError(getString(R.string.error_field_required));
            focusView = mEditName;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (appPreferences.getLoginIDUser() != null) {
                updateProfile(appPreferences.getLoginIDUser());
            }
        }
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}


