package com.beecode.flashmotorizado.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.beecode.flashmotorizado.R;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private long splashDelay = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent mainIntent;
                mainIntent = new Intent().setClass(SplashActivity.this, LoginActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainIntent);
                finish();//Destruimos esta activity para prevenir que el usuario retorne aqui presionando el boton Atras.

            }
        };

        Timer timer = new Timer();
        timer.schedule(task, splashDelay);//Pasado los 5 segundos dispara la tarea
    }
}
