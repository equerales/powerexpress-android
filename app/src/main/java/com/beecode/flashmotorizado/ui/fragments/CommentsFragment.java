package com.beecode.flashmotorizado.ui.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.Util;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by enny.querales on 5/7/2016.
 */
public class CommentsFragment extends DialogFragment {

    private EditText mEditComments;
    private Button mButtonSave;
    private Button mButtonCancel;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface CommentsFragmentDialogListener {
        public void onDialogComments(String comments);
    }

    private CommentsFragmentDialogListener mListener;

    public static CommentsFragment newInstance(CommentsFragmentDialogListener mListener) {

        CommentsFragment fragment = new CommentsFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_comments);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mEditComments = (EditText) dialog.findViewById(R.id.dialog_comments);
        mEditComments.setHorizontallyScrolling(false);
        mEditComments.setLines(14);
        mEditComments.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    Util.hideKeyboard(mEditComments, getActivity());
                    return true;
                } else {
                    return false;
                }
            }
        });
        mButtonSave = (Button) dialog.findViewById(R.id.comments_save);
        mButtonCancel = (Button) dialog.findViewById(R.id.comments_cancel);
        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mEditComments.getText().toString().isEmpty()) {
                    mListener.onDialogComments(mEditComments.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;
    }
}


