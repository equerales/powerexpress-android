package com.beecode.flashmotorizado.ui.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.interfaces.LoginTaskCompleted;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.tasks.LoginAsyncTask;
import com.beecode.flashmotorizado.ui.LoginActivity;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.beecode.flashmotorizado.util.Util;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by enny.querales on 23/6/2016.
 */
public class ChangePasswordFragment extends DialogFragment implements LoginTaskCompleted {

    private EditText mPassword;
    private EditText mRepeatPassword;
    private EditText mPasswordNow;
    private Button mSave;
    private Button mCancel;
    private String password;
    private String repeatPassword;
    private String passwordNow;
    private View mProgressView;
    private AppPreferences appPreferences;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface ChangePasswordFragmentDialogListener {
        public void onDialoChangePassword(String password);
    }

    private ChangePasswordFragmentDialogListener mListener;

    public static ChangePasswordFragment newInstance(ChangePasswordFragmentDialogListener mListener) {

        ChangePasswordFragment fragment = new ChangePasswordFragment();
        fragment.mListener = mListener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_change_password);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        appPreferences = new AppPreferences(getActivity());
        mPasswordNow = (EditText) dialog.findViewById(R.id.change_password_now);
        mPassword = (EditText) dialog.findViewById(R.id.change_password_pass);
        mRepeatPassword = (EditText) dialog.findViewById(R.id.change_password_repeat);
        mSave = (Button) dialog.findViewById(R.id.change_password_save);
        mCancel = (Button) dialog.findViewById(R.id.change_password_cancel);
        mProgressView = dialog.findViewById(R.id.progress_bar);

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptChangePassword();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyboard(mPassword, getActivity());
                Util.hideKeyboard(mRepeatPassword, getActivity());
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    private void attemptChangePassword() {

        // Reset errors.
        mPassword.setError(null);
        mRepeatPassword.setError(null);
        mPasswordNow.setError(null);

        // Store values at the time of the login attempt.
        password = mPassword.getText().toString();
        repeatPassword = mRepeatPassword.getText().toString();
        passwordNow = mPasswordNow.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(mRepeatPassword.getText().toString()) && !TextUtils.isEmpty(mPassword.getText().toString())) {
            if (!mRepeatPassword.getText().toString().equals(mPassword.getText().toString())) {
                mRepeatPassword.setError(getString(R.string.password_not_math));
                focusView = mRepeatPassword;
                cancel = true;
            }
        }

        if (!TextUtils.isEmpty(mRepeatPassword.getText().toString()) && !Util.isPasswordValid(mRepeatPassword.getText().toString())) {
            mRepeatPassword.setError(getString(R.string.password_too_short));
            focusView = mRepeatPassword;
            cancel = true;
        }


        if (!TextUtils.isEmpty(mPassword.getText().toString()) && !Util.isPasswordValid(mPassword.getText().toString())) {
            mPassword.setError(getString(R.string.password_too_short));
            focusView = mPassword;
            cancel = true;
        }

        if (!TextUtils.isEmpty(mPasswordNow.getText().toString()) && !Util.isPasswordValid(mPasswordNow.getText().toString())) {
            mPasswordNow.setError(getString(R.string.password_too_short));
            focusView = mPasswordNow;
            cancel = true;
        }

        if (TextUtils.isEmpty(repeatPassword)) {
            mRepeatPassword.setError(getString(R.string.error_field_required));
            focusView = mRepeatPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPassword.setError(getString(R.string.error_field_required));
            focusView = mPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordNow)) {
            mPasswordNow.setError(getString(R.string.error_field_required));
            focusView = mPasswordNow;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (appPreferences.getLoginIDUser() != null) {
                LoginAsyncTask mTask = new LoginAsyncTask(getActivity(), ChangePasswordFragment.this);
                mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, appPreferences.getLoginEmailUser().toString(), mPasswordNow.getText().toString());
            }
        }
    }


    public void updateProfile(String idDeliveryMan) {
        showProgress(true);
        String url = getResources().getString(R.string.url_services_app) + "/deliveryman/" + idDeliveryMan;
        System.out.println(url);
        Map<String, String> params = new HashMap<String, String>();
        params.put("password", mRepeatPassword.getText().toString());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        System.out.println(url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String mMessage = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();
                            Log.d("answer", response.toString());
                            showProgress(false);
                            Util.hideKeyboard(mPassword, getActivity());
                            Util.hideKeyboard(mRepeatPassword, getActivity());
                            Util.hideKeyboard(mPasswordNow, getActivity());
                            if (mSuccess) {
                                dismiss();
                                appPreferences.removePreferences();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                getActivity().finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    showProgress(false);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                Crashlytics.logException(error);
                VolleyLog.d("answer", "Error: " + error.getMessage());
            }

        }) {
        };
        queue.add(jsonObjReq);
    }

    @Override
    public void onLoginTaskStart() {
        showProgress(true);
    }

    @Override
    public void onLoginTaskCompleted(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {
                boolean success = jsonObject.getBoolean(Codes.FIELD_SUCCESS);
                String message = jsonObject.getString(Codes.FIELD_MESSAGE);
                Log.i("Respuesta Server ", jsonObject.toString());
                if (success) {
                    updateProfile(appPreferences.getLoginIDUser());
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.pass_now_invalid), Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            } else {
                Toast.makeText(getActivity(), R.string.no_response, Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e("", "Push message json exception: " + e.getMessage());
            showProgress(false);
        }
    }

}

