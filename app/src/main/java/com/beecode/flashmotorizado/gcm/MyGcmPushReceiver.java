/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beecode.flashmotorizado.gcm;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.beecode.flashmotorizado.ui.MainActivity;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;


public class MyGcmPushReceiver extends GcmListenerService {

    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();
    private AppPreferences appPreferences;

    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param from   SenderID of the sender.
     * @param bundle Data bundle containing message data as key/value pairs.
     *               For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(String from, Bundle bundle) {
        String title = bundle.getString("title");
        Boolean isBackground = Boolean.valueOf(bundle.getString("is_background"));
        String flag = bundle.getString("flag");
        String data = bundle.getString("data");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "title: " + title);
        Log.d(TAG, "isBackground: " + isBackground);
        Log.d(TAG, "flag: " + flag);
        Log.d(TAG, "data: " + data);

        if (flag == null)
            return;
        processUserMessage(title, isBackground, data);
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */
    private void processUserMessage(String title, boolean isBackground, String data) {
        if (!isBackground) {

            try {
                JSONObject datObj = new JSONObject(data);

                JSONObject mObj = datObj.getJSONObject("message");
                String message = mObj.getString("message");
                String date = datObj.getString("created_at");
                String customer = datObj.getString("customer");
                String store_photo = datObj.getString("store_photo");
                String store_name = datObj.getString("store_name");
                String order_id = datObj.getString("order");
                String distance = datObj.getString("distance");
                String shipping = datObj.getString("moto_profit");


                System.out.println("customer: " + customer);
                System.out.println("store_photo: " + store_photo);
                System.out.println("store_name: " + store_name);
                System.out.println("order: " + order_id);
                System.out.println("distance: " + distance);
                System.out.println("shipping: " + shipping);
                // verifying whether the app is in background or foreground
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                    // app is in foreground, broadcast the push message
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    pushNotification.putExtra("type", Config.PUSH_TYPE_USER);
                    pushNotification.putExtra("title", title);
                    pushNotification.putExtra("message", message);
                    pushNotification.putExtra("customer", customer);
                    pushNotification.putExtra("store_photo", store_photo);
                    pushNotification.putExtra("store_name", store_name);
                    pushNotification.putExtra("order", order_id);
                    pushNotification.putExtra("distance", distance);
                    pushNotification.putExtra("shipping", shipping);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    // play notification sound
                    playNotificationSound(getApplicationContext());
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(500);
                } else {
                    // app is in background. show the message in notification try
                    Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    resultIntent.putExtra("title", title);
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("customer", customer);
                    resultIntent.putExtra("store_photo", store_photo);
                    resultIntent.putExtra("store_name", store_name);
                    resultIntent.putExtra("order", order_id);
                    resultIntent.putExtra("distance", distance);
                    resultIntent.putExtra("shipping", shipping);
                    showNotificationMessage(getApplicationContext(), title, message, date, resultIntent);
                }
            } catch (JSONException e) {
                Crashlytics.logException(e);
                Log.e(TAG, "json parsing error: " + e.getMessage());
            }
        } else {
            // the push notification is silent, may be other operations needed
            // like inserting it in to SQLite
        }
    }


    /**
     * Processing chat room push message
     * this message will be broadcasts to all the activities registered
     * */

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     * */
    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    public void playNotificationSound(Context context) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getApplicationContext().getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(context.getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }
}
