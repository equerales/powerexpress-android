package com.beecode.flashmotorizado.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.DetailPedido;

import java.util.List;

/**
 * Created by Enny on 27/10/2016.
 */

public class ListAdapterDetailtems extends BaseAdapter {

    private Activity mActivity;
    private List<DetailPedido> result;
    private int layoutView;

    public ListAdapterDetailtems(Activity context, List<DetailPedido> result, int layoutView) {
        this.result = result;
        this.mActivity = context;
        this.layoutView = layoutView;
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.get(position);
    }

    @Override
    public long getItemId(int position) {
        return result.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layoutView, null);
        }

        final ViewHolder holder;
        holder = new ViewHolder();

        holder.textViewProduct = (TextView) convertView.findViewById(R.id.detail_product);
        holder.textViewPrice = (TextView) convertView.findViewById(R.id.detail_price);
        holder.textViewQuantity = (TextView) convertView.findViewById(R.id.detail_quantity);

        if (holder.textViewQuantity != null)
            holder.textViewQuantity.setText(String.valueOf(result.get(position).getQuantity()));

        String note = "";
        if (result.get(position).getNote() != null) {
            if (!result.get(position).getNote().equalsIgnoreCase("null")) {
                note = result.get(position).getNote();
            }
        }

        if (holder.textViewProduct != null)
            holder.textViewProduct.setText(result.get(position).getProduct().getName() + "" + note);

        if (holder.textViewPrice != null)
            holder.textViewPrice.setText(result.get(position).getProduct().getPrice() + "$");

        return convertView;
    }


    public class ViewHolder {
        public TextView textViewProduct;
        public TextView textViewPrice;
        public TextView textViewQuantity;
    }
}

