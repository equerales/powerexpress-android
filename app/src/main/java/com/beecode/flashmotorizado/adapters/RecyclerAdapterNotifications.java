package com.beecode.flashmotorizado.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.Pedido;
import com.beecode.flashmotorizado.remote.Codes;
import com.beecode.flashmotorizado.ui.DetailOrderActivity;
import com.beecode.flashmotorizado.ui.MainActivity;
import com.beecode.flashmotorizado.ui.StatusMotoActivity;
import com.beecode.flashmotorizado.util.AppPreferences;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;


/**
 * Created by enny.querales on 1/6/2016.
 */
public class RecyclerAdapterNotifications extends RecyclerView.Adapter<RecyclerAdapterNotifications.ViewHolder> {

    private Activity mActivity;
    private List<Pedido> result;
    private AppPreferences appPreferences;
    private HashMap<TextView, CountDownTimer> counters;

    public RecyclerAdapterNotifications(Activity context, List<Pedido> result) {
        this.result = result;
        this.mActivity = context;
        appPreferences = new AppPreferences(context);
        this.counters = new HashMap<TextView, CountDownTimer>();
    }

    public void addAll(List<Pedido> list) {
        result.addAll(list);
        notifyDataSetChanged();
    }

    public void addPedido(Pedido pedido) {
        result.add(pedido);
        notifyDataSetChanged();
    }

    public void removePedido(Pedido pedido) {
        result.remove(pedido);
        notifyDataSetChanged();
        ((MainActivity) mActivity).changeNotification(result.size());
    }


    public void clear() {
        result.clear();
        notifyDataSetChanged();
    }


    @Override
    public RecyclerAdapterNotifications.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_detail_notifications, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterNotifications.ViewHolder holder, final int position) {

        if (holder.textViewRestaurant != null)
            holder.textViewRestaurant.setText(result.get(position).getRestaurant().getName());

        if (holder.textViewTip != null)
            holder.textViewTip.setText("Comisión: " + String.valueOf(result.get(position).getShip()));

        if (holder.textViewDistance != null)
            holder.textViewDistance.setText("A " + result.get(position).getDistance() + " Km");

        if (holder.imageEstablishment != null) {
            if (!result.get(position).getRestaurant().getPhoto().isEmpty()) {
                Picasso.with(mActivity).load(mActivity.getResources().getString(R.string.url_image) + result.get(position).getRestaurant().getPhoto())
                        .resize(200, 200)
                        .into(holder.imageEstablishment);
            } else {
                holder.imageEstablishment.setBackgroundResource(R.drawable.restaurant);
            }
        }


        if (holder.buttonCancelOrder != null) {
            holder.buttonCancelOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removePedido(result.get(position));
                }
            });
        }

        if (holder.buttonAcceptOrder != null) {
            holder.buttonAcceptOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setDeliveryManOrder(result.get(position).getId(), result.get(position));
                }
            });
        }

        if (holder.buttonDetails != null) {
            holder.buttonDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.startActivity(new Intent(mActivity, DetailOrderActivity.class)
                            .putExtra("pedido", result.get(position)));
                }
            });
        }

        CountDownTimer cdt = counters.get(holder.mTextTimer);
        if (cdt != null) {
            cdt.cancel();
            cdt = null;
        }

        cdt = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = 0;
                String sDate = "";


                if (millisUntilFinished > DateUtils.SECOND_IN_MILLIS) {
                    seconds = (int) (millisUntilFinished / DateUtils.SECOND_IN_MILLIS);
                }

                sDate += String.format("%02d", seconds);
                holder.mTextTimer.setText(sDate.trim());
            }

            @Override
            public void onFinish() {
                counters.remove(holder.mTextTimer);
                if (result.size() > position) {
                    result.remove(position);
                    notifyDataSetChanged();
                    ((MainActivity) mActivity).changeNotification(result.size());
                }
            }
        };

        counters.put(holder.mTextTimer, cdt);
        cdt.start();
    }

    @Override
    public int getItemCount() {
        return result.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewRestaurant;
        public TextView textViewTip;
        public TextView textViewDistance;
        public ImageView imageEstablishment;
        private Button buttonCancelOrder;
        private Button buttonAcceptOrder;
        private Button buttonDetails;
        private ProgressBar mProgressView;
        private TextView mTextTimer;


        public ViewHolder(View mView) {
            super(mView);
            textViewRestaurant = (TextView) mView.findViewById(R.id.notification_establishment);
            textViewDistance = (TextView) mView.findViewById(R.id.notification_distance);
            textViewTip = (TextView) mView.findViewById(R.id.notification_comision);
            buttonAcceptOrder = (Button) mView.findViewById(R.id.notification_accept_order);
            buttonCancelOrder = (Button) mView.findViewById(R.id.notification_cancel_order);
            buttonDetails = (Button) mView.findViewById(R.id.notification_detail);
            mProgressView = (ProgressBar) mView.findViewById(R.id.progress_bar);
            mTextTimer = (TextView) mView.findViewById(R.id.progressText);

        }

    }


    public void setDeliveryManOrder(String idOrder, final Pedido ped) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", "Asignando Orden...", true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/order/assign";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_DELIVERYMAN_ID, appPreferences.getLoginIDUser());
            jsonObject.accumulate(Codes.FIELD_ID, idOrder);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        System.out.println(url);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        try {
                            boolean mSuccess = response.getBoolean(Codes.FIELD_SUCCESS);
                            String message = response.getString(Codes.FIELD_MESSAGE);
                            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                            removePedido(ped);
                            dialog.dismiss();
                            if (mSuccess) {
                                ped.setStatus(1);
                                mActivity.startActivity(new Intent(mActivity, StatusMotoActivity.class).putExtra("pedido", ped));
                                sendPushStatusClient(ped);
                            }
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.no_response), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);
    }

    public void sendPushStatusClient(final Pedido ped) {
        final ProgressDialog dialog = ProgressDialog.show(mActivity, "", mActivity.getResources().getString(R.string.sending_notification), true);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        String url = "https://apirestfm.herokuapp.com/api/push/tocustomer";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Codes.FIELD_RECIPIENTS, ped.getCustomer().getId());
            jsonObject.accumulate(Codes.FIELD_TITLE, "Tu Orden Ha sido Aceptada");
            jsonObject.accumulate(Codes.FIELD_MESSAGE, (appPreferences.getLoginNameUser() + " ACEPTÓ TU ORDEN").toUpperCase());
            jsonObject.accumulate(Codes.FIELD_STATUS, 1);
            jsonObject.accumulate(Codes.FIELD_DELIVERYMAN, appPreferences.getLoginIDUser());
            jsonObject.accumulate(Codes.FIELD_ORDER, ped.getId());
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("answer", response.toString());
                        dialog.dismiss();
                        try {
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.i("RESPUESTA CON ERROR", error.getMessage());
                    Toast.makeText(mActivity, R.string.no_response, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }
        }) {
        };
        queue.add(jsonObjReq);
    }

}

