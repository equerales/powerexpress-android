package com.beecode.flashmotorizado.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beecode.flashmotorizado.R;
import com.beecode.flashmotorizado.local.Pedido;

import java.util.List;

/**
 * Created by enny.querales on 11/6/2016.
 */
public class RecyclerAdapterSummary extends RecyclerView.Adapter<RecyclerAdapterSummary.ViewHolder> {

    private Activity mActivity;
    private List<Pedido> result;
    private static ClickListener clickListener;

    public RecyclerAdapterSummary(Activity context, List<Pedido> result) {
        this.result = result;
        this.mActivity = context;
    }


    @Override
    public RecyclerAdapterSummary.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_detail_summary, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerAdapterSummary.ViewHolder holder, final int position) {

        if (holder.textViewRestaurant != null)
            holder.textViewRestaurant.setText(result.get(position).getRestaurant().getName());

        if (holder.textViewTotal != null)
            holder.textViewTotal.setText(String.valueOf(result.get(position).getTotal()));

        if (holder.textViewCustomer != null)
            holder.textViewCustomer.setText(result.get(position).getCustomer().getFirst_name());

        if (holder.textViewComision != null)
            holder.textViewComision.setText(String.valueOf(result.get(position).getTax()));

        if (holder.textViewStatus != null) {
            if (result.get(position).getStatus() == 0) {
                holder.textViewStatus.setText("NO ATENDIDA");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.color_dark_grey));
            }

            if (result.get(position).getStatus() == 1) {
                holder.textViewStatus.setText("ACEPTADA");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.colorGreen));
            }


            if (result.get(position).getStatus() == 2) {
                holder.textViewStatus.setText("HACIA EL ESTABLECIMIENTO");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.colorRed));
            }

            if (result.get(position).getStatus() == 3) {
                holder.textViewStatus.setText("EN EL ESTABLECIMIENTO");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.colorRed));
            }

            if (result.get(position).getStatus() == 4) {
                holder.textViewStatus.setText("CAMINO AL CLIENTE");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.colorRed));
            }

            if (result.get(position).getStatus() >= 5) {
                holder.textViewStatus.setText("ENTREGADA");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.colorGreen));
            }

            if (result.get(position).getCancelled() == 1) {
                holder.textViewStatus.setText("CANCELADA");
                holder.textViewStatus.setTextColor(mActivity.getResources().getColor(R.color.color_dark_grey));
            }
        }
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textViewRestaurant;
        public TextView textViewTotal;
        public TextView textViewCustomer;
        public TextView textViewComision;
        public TextView textViewStatus;


        public ViewHolder(View mView) {
            super(mView);
            mView.setOnClickListener(this);
            textViewRestaurant = (TextView) mView.findViewById(R.id.summary_restaurant);
            textViewTotal = (TextView) mView.findViewById(R.id.summary_total);
            textViewCustomer = (TextView) mView.findViewById(R.id.summary_customer);
            textViewComision = (TextView) mView.findViewById(R.id.summary_comision);
            textViewStatus = (TextView) mView.findViewById(R.id.summary_status);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerAdapterSummary.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}

