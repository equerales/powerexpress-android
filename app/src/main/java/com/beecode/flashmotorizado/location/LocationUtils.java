package com.beecode.flashmotorizado.location;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public final class LocationUtils {  
    // 
    public static final long ONE_MIN = 1000 * 60;
    public static final long TWO_MIN = ONE_MIN * 2;
    public static final long FIVE_MIN = ONE_MIN * 5;
    public static final long POLLING_FREQ = 1000 * 30;
    public static final long FASTEST_UPDATE_FREQ = 1000 * 5;
    public static final float MIN_ACCURACY = 25.0f;
    public static final float MIN_LAST_READ_ACCURACY = 500.0f;
    
	public static boolean servicesAvailable(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, 0).show();
            return false;
        }
    }
	
}
