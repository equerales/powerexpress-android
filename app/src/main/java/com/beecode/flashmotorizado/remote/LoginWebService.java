package com.beecode.flashmotorizado.remote;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Enny on 10/6/2016.
 */
public class LoginWebService extends WebService {
    private final String TAG = LoginWebService.class.getSimpleName();
    //private final String webServiceDomain = "http://apirestfm.herokuapp.com/api/deliveryman/login";
    private String webServiceDomain;

    public LoginWebService() {
        super();
    }

    public LoginWebService(String webServiceDomain) {
        super();
        this.webServiceDomain = webServiceDomain;
    }

    public String login(String email, String pass) {
        String json = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate(Codes.FIELD_EMAIL, email);
            jsonObject.accumulate(Codes.FIELD_PASSWORD, pass);
            json = jsonObject.toString();
        } catch (JSONException je) {
            Crashlytics.logException(je);
            Log.e(TAG, "Push message json exception: " + je.getMessage());
        }
        return super.doHttpPost(json, webServiceDomain);
    }
}

