package com.beecode.flashmotorizado.remote;

/**
 * Created by enny.querales on 9/6/2016.
 */

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by enny.querales on 24/10/2015.
 */
public abstract class WebService {
    private final String TAG = WebService.class.getSimpleName();
    private final int MAX_TRIES = 5;

    public WebService() {
    }

    protected String doHttpPost(String json, String webServiceDomain) {
        HttpParams httpParameters;
        HttpClient httpClient;
        HttpPost httpPost;
        HttpResponse response;
        HttpEntity entity;
        String res = "";
        int tries = 0;

        while (tries < MAX_TRIES) {
            tries++;
            try {
                System.out.println("json enviado : " + json);
                for (int i = 0; i < json.length(); i += 1024) {
                    if (i + 1024 < json.length())
                        Log.e("PRIMER ENCODED", json.substring(i, i + 1024));
                    else
                        Log.e("PRIMER ENCODED", json.substring(i, json.length()));
                }
                int timeoutConnection = 8000;
                int timeoutSocket = 8000;
                httpParameters = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
                httpClient = new DefaultHttpClient(httpParameters);
                httpPost = new HttpPost(webServiceDomain);
                StringEntity se = new StringEntity(json, HTTP.UTF_8);
                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                response = httpClient.execute(httpPost);
                entity = response.getEntity();
                BufferedReader b = new BufferedReader(new InputStreamReader(entity.getContent()));
                String line;
                while ((line = b.readLine()) != null) {
                    res += (line + "\n");
                }
                Log.i("Server response", res);
                b.close();
                entity.consumeContent();
                httpClient.getConnectionManager().shutdown();
                tries = MAX_TRIES;
                Log.i("Server response", res);
            } catch (ClientProtocolException e) {
                Crashlytics.logException(e);
                Log.e(TAG, "Push message json exception: " + res);
            } catch (IOException e) {
                Crashlytics.logException(e);
                Log.e(TAG, "Push message json exception: " + res);
            }
        }
        Log.i("Server response", res);
        return res;
    }

}
