package com.beecode.flashmotorizado.remote;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by enny.querales on 9/6/2016.
 */
public class AccountWebService extends WebService {
    private final String TAG = AccountWebService.class.getSimpleName();
    //private final String webServiceDomain = "http://apirestfm.herokuapp.com/api/deliveryman";
    private String webServiceDomain;

    public AccountWebService() {
        super();
    }

    public AccountWebService(String webServiceDomain) {
        super();
        this.webServiceDomain = webServiceDomain;
    }

    public String signUp(String name, String email, String pass) {
        String json = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate(Codes.FIELD_FIRST_NAME, name);
            jsonObject.accumulate(Codes.FIELD_EMAIL, email);
            jsonObject.accumulate(Codes.FIELD_PASSWORD, pass);
            json = jsonObject.toString();
        } catch (JSONException je) {
            Crashlytics.logException(je);
            Log.e(TAG, "Push message json exception: " + je.getMessage());
        }
        return super.doHttpPost(json, webServiceDomain);
    }
}

