package com.beecode.flashmotorizado.remote;

/**
 * Created by enny.querales on 1/6/2016.
 */
public class Codes {
    public static String FIELD_ACTIVE = "active";
    public static String FIELD_SUCCESS = "success";
    public static String FIELD_MESSAGE = "message";
    public static String FIELD_RESULT = "result";
    public static String FIELD_DETAILS = "details";
    public static String FIELD_ID = "id";
    public static String FIELD_ID_CATEGORY = "category_id";
    public static String FIELD_NAME = "name";
    public static String FIELD_PHONE = "phone";
    public static String FIELD_FAX = "fax";
    public static String FIELD_LATITUDE = "lat";
    public static String FIELD_LONGITUDE = "lng";
    public static String FIELD_PHOTO = "photo";
    public static String FIELD_CREATED_DATE = "created_at";
    public static String FIELD_UPDATED_DATE = "updated_at";
    public static String FIELD_NAME_CATEGORY = "category_name";
    public static String FIELD_ID_DEPARTMENT = "department_id";
    public static String FIELD_NAME_DEPARTMENT = "department_name";
    public static String FIELD_DEPARTMENT = "department";
    public static String FIELD_ID_PRODUCT = "product_id";
    public static String FIELD_DESCRIPTION = "description";
    public static String FIELD_RESTAURANT_ID = "restaurant_id";
    public static String FIELD_RESTAURANT_NAME = "restaurant_name";
    public static String FIELD_STORE_ID = "store_id";
    public static String FIELD_REFERENCE = "reference";
    public static String FIELD_PRICE = "price";
    public static String FIELD_PRODUCT = "product";
    public static String FIELD_PHOTO_PATH = "path_photo";
    public static String FIELD_PHOTO_STORE = "store_photo";
    public static String FIELD_ADDRESS_STORE = "store_address";
    public static String FIELD_NOTE = "note";



    public static String FIELD_FIRST_NAME = "first_name";
    public static String FIELD_LAST_NAME = "last_name";
    public static String FIELD_EMAIL = "email";
    public static String FIELD_PASSWORD = "password";
    public static String FIELD_LAT = "lat";
    public static String FIELD_LNG = "lng";

    public static String FIELD_CARD_NAME = "name_on_card";
    public static String FIELD_EXP_DATE = "exp_date";
    public static String FIELD_CCV = "ccv";
    public static String FIELD_TYPE = "type";
    public static String FIELD_USER_ID = "user_id";
    public static String FIELD_CUSTOMER_ID = "customer_id";

    public static String FIELD_ADDRESS = "address";
    public static String FIELD_CUSTOMER_ADDRESS = "customer_address";
    public static String FIELD_CUSTOMER_PHONE = "customer_phone";
    public static String FIELD_ORDER_ID = "order_id";
    public static String FIELD_INVOICE = "invoice";
    public static String FIELD_ORDER = "order";
    public static String FIELD_ADDRESS_ID = "address_id";
    public static String FIELD_CARD_ID = "card_id";
    public static String FIELD_CITY = "city";
    public static String FIELD_PROVINCE = "province";
    public static String FIELD_ZIPCODE = "zipcode";
    public static String FIELD_CARDS = "cards";
    public static String FIELD_QUANTITY = "quantity";
    public static String FIELD_AMOUNT = "amount";
    public static String FIELD_SUBTOTAL = "subtotal";
    public static String FIELD_TAX = "tax";
    public static String FIELD_SHIP = "ship";
    public static String FIELD_TOTAL = "total";
    public static String FIELD_CUSTOMER = "customer";
    public static String FIELD_CUSTOMER_NAME = "customer_name";
    public static String FIELD_STORE = "store";
    public static String FIELD_STORE_NAME = "store_name";
    public static String FIELD_CARD_NUMBER = "card_number";
    public static String FIELD_CUSTOMER_CARD_NUMBER = "customer_card";
    public static String FIELD_DELIVERYMAN_ID = "deliveryman_id";
    public static String FIELD_DELIVERYMAN = "deliveryman";
    public static String FIELD_RECIPIENTS = "recipients";
    public static String FIELD_TITLE = "title";
    public static String FIELD_ORDERS_HISTORY = "orders_history";
    public static String FIELD_STATUS = "status";
    public static String FIELD_CURRENT_STATUS = "current_status";
    public static String FIELD_CANCELLED = "cancelled";
    public static String FIELD_TOKEN = "token";
}
